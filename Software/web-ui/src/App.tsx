import './App.css';

import { useState, useCallback } from "react";

import { Login, isTokenValid } from './views/Login';

import Dashboard from './views/Dashboard';

function App() {

  const[isLoggedin, setLoggedin] = useState<boolean>(false);

  const updateLoginState = useCallback(val => {
    setLoggedin(val);
  }, [setLoggedin]);

  return (
    <div className="App">
      
       {
        isTokenValid() ?
          <Dashboard updateLoginState={updateLoginState} />
        :     
          <Login updateLoginState={updateLoginState}/>      
      }
    </div>
  );
}

export default App;

/**

            logged &&
            <>
              <Header />
              <SideMenu updateView={updateView}/>
              
              <div className="App-header">
                {viewComponent}
              </div>
            </>
          }
       {
        logged ?
        <>
          <Header />
          <SideMenu updateView={updateView}/>
          
          <div className="App-header">
            {viewComponent}
          </div>

        </>
        :
        <div className="App-header">
          <Login />
        </div>
      }
 *       <div className="App-header">
        
        <Grid container direction="column">
          <Grid item xs={8} style={{ height: 70+'em' }}>
            {
              currentView === 'DISPLAY' ? <EntityTable /> : (currentView === 'ADD' ? <DataGenerator /> : '')
            }
          </Grid>
        </Grid>
        <Grid item container direction="row" justifyContent="space-around">
          <Grid item>
            <Button onClick={() => setCurrentView('ADD')} size="large" variant="contained" color={currentView === 'ADD' ? "primary" : "secondary"}>Add</Button>
          </Grid>
          <Grid item>
            <Button onClick={() => setCurrentView('DISPLAY')} size="large" variant="contained" color={currentView === 'DISPLAY' ? "primary" : "secondary"}>Display</Button>
          </Grid>
        </Grid>
      </div>
 */