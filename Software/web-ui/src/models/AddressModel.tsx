
export default interface IAdress {
    id?: number;
    street: string;
    houseNumber: string;
    city: ICity;
}

export interface ICity {
    id?: number;
    name: string;
    postalCode: string;  
}