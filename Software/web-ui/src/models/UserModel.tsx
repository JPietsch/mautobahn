
import IAddress from '../models/AddressModel';

export default interface IUser {
    id: number;
    email?: string;
    username: string;
    password?: string;
    firstName?: string
    lastName?: string;
    address?: IAddress;
    roles?: Array<string>;
}