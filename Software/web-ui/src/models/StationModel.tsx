
export default interface IStation {
    id?: number;
    title: string;
    latitude: string | number;
    longitude: string | number;
    distance: string | number;
    previous?: string | number | null;
    type: "DEPARTURE" | "PASSAGE";
}
