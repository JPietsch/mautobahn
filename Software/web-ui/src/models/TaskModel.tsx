import IStation from "./StationModel";
import IUser from "./UserModel";
import IVehicle from "./VehicleModel";

export default interface ITask {
    title: string;
    description: string;
    requestType: "GET" | "POST";
    url: string;
    object?: IUser | IVehicle | IStation;
    objectType?: 'User' | 'Vehicle' | 'Station' | 'Action',
    objectAmount?: number;
    
    state: "success" | "running" | "failed" | "disabled";
}

export interface ITestCase {
    title: string;
    description: string;
    reqiurements: Array<ITask>;
    tests: Array<ITask>;
    results: Array<ITask>;
}

export interface ILocation {
    title: string;
    latitude: number;
    longitude: number;
}

export const TaskTemplate:ITask = {
    title: "Neue Aufgabe",
    description: "Eine neue Aufagbe",
    requestType: "GET",
    url: "/stations/all",
    state: "disabled",
    objectAmount: 1
}

export const TestCaseTemplate:ITestCase = {
    title: "Neuer Test",
    description: "Ein neuer Test",
    reqiurements: [],
    tests: [],
    results: []
}


export const TestCase:ITestCase = {
    title: "Teste Fahrt",
    description: "Es wird eine Fahrt simuliert",
    reqiurements: [
        {
            title: "Erstelle Nutzer",
            description: "Nutzer erstellen, der mir seinem Auto dirch die Gegend pferd.",
            requestType: "POST",
            url: "user/save",
            state: "disabled",
            objectType: "User",
            objectAmount: 1
        },
        {
            title: "Erstelle Fahrzeug",
            description: "Erstelle ein Auto, mit dem der Nutzer fahren kann.",
            requestType: "POST",
            url: "vehicle/save",
            state: "disabled",
            objectType: "Vehicle",
            objectAmount: 1
        },
        {
            title: "Erstelle Stationen",
            description: "Erstelle ein paar Stationen.",
            requestType: "POST",
            url: "station/save",
            state: "disabled",
            objectType: "Station",
            objectAmount: 5
        }
    ],
    tests: [
        {
            title: "Simuliere Auffahrt",
            description: "Nutzer pferd auf Autobahn.",
            requestType: "POST",
            url: "action/update",
            state: "disabled",
            objectType: "Action",
            objectAmount: 1
        },
        {
            title: "Simuliere Durchfahrt",
            description: "Nutzer pferd durch Checkpoint.",
            requestType: "POST",
            url: "action/update",
            state: "disabled",
            objectType: "Action",
            objectAmount: 3
        },
        {
            title: "Simuliere Abfahrt",
            description: "Nutzer pferd von der Autobahn.",
            requestType: "POST",
            url: "action/update",
            state: "disabled",
            objectType: "Action",
            objectAmount: 1
        }
    ],
    results: [
        {
            title: "Hole Fahrten",
            description: "Gucken, ob ne Strecke erstellt wurde.",
            requestType: "GET",
            url: "track/",
            state: "disabled",
        }
    ]
}

export const allLoc = [ { "title": "Bautzen", "latitude": 51.18035, "longitude": 14.43494 }, { "title": "Dresden N", "latitude": 51.050408, "longitude": 13.737262 }, { "title": "Dresden SO", "latitude": 51.050406, "longitude": 13.737263 }, { "title": "Dresden W", "latitude": 51.050407, "longitude": 13.737261 }, { "title": "Hamburg Nord", "latitude": 53.551087, "longitude": 9.993682 }, { "title": "Hamburg Ost", "latitude": 53.551086, "longitude": 9.993683 }, { "title": "Hamburg Süd", "latitude": 53.551085, "longitude": 9.993682 }, { "title": "Flensburg", "latitude": 54.7833021, "longitude": 9.4333264 }, { "title": "Schwerin", "latitude": 53.6288297, "longitude": 11.4148038 }, { "title": "Hannover N", "latitude": 52.3744780, "longitude": 9.7385532 }, { "title": "Hannover O", "latitude": 52.3744779, "longitude": 9.7385533 }, { "title": "Hannover S", "latitude": 52.3744778, "longitude": 9.7385532 }, { "title": "Berlin W", "latitude": 52.5170365, "longitude": 13.3888598 }, { "title": "Berlin SW", "latitude": 52.5170364, "longitude": 13.3888598 }, { "title": "Berlin S", "latitude": 52.5170365, "longitude": 13.3888598 }, { "title": "Kassel NO", "latitude": 51.3154547, "longitude": 9.4924097 }, { "title": "Kassel O", "latitude": 51.3154546, "longitude": 9.4924097 }, { "title": "Kassel S", "latitude": 51.3154546, "longitude": 9.4924095 }, { "title": "Köln N", "latitude": 50.938362, "longitude": 6.959974 }, { "title": "Köln W", "latitude": 50.938361, "longitude": 6.959973 }, { "title": "Köln SO", "latitude": 50.938360, "longitude": 6.959975 }, { "title": "Aachen", "latitude": 50.776351, "longitude": 6.083862 }, { "title": "Würzburg", "latitude": 49.79245, "longitude": 9.932966 }, { "title": "Frankfurt NW", "latitude": 50.1106445, "longitude": 8.6820916 }, { "title": "Frankfurt N", "latitude": 50.1106445, "longitude": 8.6820917 }, { "title": "Frankfurt NO", "latitude": 50.1106445, "longitude": 8.6820918 }, { "title": "Frankfurt SW", "latitude": 50.1106443, "longitude": 8.6820916 }, { "title": "Frankfurt S", "latitude": 50.1106443, "longitude": 8.6820917 }, { "title": "Mainz", "latitude": 50.0012314, "longitude": 8.2762513 }, { "title": "Stuttgart", "latitude": 48.7784485, "longitude": 9.1800132 }, { "title": "München", "latitude": 48.1371079, "longitude": 11.5753822 }, { "title": "Lindau", "latitude": 47.550753, "longitude": 9.6926624 } ]

// Das is kein Pfehler!
export const Pfahrten = [
//    [
//        55, 52, 47, 42, 37, 24
//    ],
    [
        27, 32, 35, 50
    ],
    [
        65, 63, 59, 54, 47, 42, 37, 24, 19, 14, 9, 16
    ],
    [
        43, 40, 41, 48, 67, 46
    ],
    [
        17, 12, 9, 16
    ],
    [
        35, 50, 53, 57, 62
    ],
    [
        15, 10, 13, 20, 21, 20
    ],

]

export const tracks = [
[{
    "title": "Bautzen",
    "latitude": 51.18035,
    "longitude": 14.43494
}, {
    "title": "Dresden",
    "latitude": 51.050407,
    "longitude": 13.737262
}],
[{
"title": "Berlin",
"latitude": 52.5170365,
"longitude": 13.3888599
},{
"title": "Bautzen",
"latitude": 51.18035,
"longitude": 14.43494
}],
[{
"title": "Köln",
"latitude": 50.938361,
"longitude": 6.959974
},{
"title": "Stuttgart",
"latitude": 48.7784485,
"longitude": 9.1800132
}],
[{
"title": "München",
"latitude": 48.1371079,
"longitude": 11.5753822
},{
"title": "Stuttgart",
"latitude": 48.7784485,
"longitude": 9.1800132
}],[ {
"title": "Mainz",
"latitude": 50.0012314,
"longitude": 8.2762513
}, {
"title": "Köln",
"latitude": 50.938361,
"longitude": 6.959974
}],
[{
"title": "Kassel",
"latitude": 51.3154546,
"longitude": 9.4924096
}, {
"title": "Dresden",
"latitude": 51.050407,
"longitude": 13.737262
}],
[{
"title": "Flensburg",
"latitude": 54.7833021,
"longitude": 9.4333264
},{
"title": "Schwerin",
"latitude": 53.6288297,
"longitude": 11.4148038
}],
[ {
"title": "Würzburg",
"latitude": 49.79245,
"longitude": 9.932966
},{
"title": "München",
"latitude": 48.1371079,
"longitude": 11.5753822
}],
[{
"title": "Schwerin",
"latitude": 53.6288297,
"longitude": 11.4148038
},{
"title": "Hannover",
"latitude": 52.3744779,
"longitude": 9.7385532
}],
[ {
"title": "Dresden",
"latitude": 51.050407,
"longitude": 13.737262
},{
"title": "Lindau",
"latitude": 47.550753,
"longitude": 9.6926624
}],
[{
"title": "Aachen",
"latitude": 50.776351,
"longitude": 6.083862
},{
"title": "Frankfurt",
"latitude": 50.1106444,
"longitude": 8.6820917
}],
[{
"title": "Bautzen",
"latitude": 51.18035,
"longitude": 14.43494
},{
"title": "Kassel",
"latitude": 51.3154546,
"longitude": 9.4924096
}]
]
export const locations:Array<ILocation> = [ 
{
    "title": "Bautzen",
    "latitude": 51.18035,
    "longitude": 14.43494
}, {
    "title": "Dresden",
    "latitude": 51.050407,
    "longitude": 13.737262
}, {
    "title": "Hamburg",
    "latitude": 53.551086,
    "longitude": 9.993682
}, {
    "title": "Flensburg",
    "latitude": 54.7833021,
    "longitude": 9.4333264
}, {
    "title": "Schwerin",
    "latitude": 53.6288297,
    "longitude": 11.4148038
}, {
    "title": "Hannover",
    "latitude": 52.3744779,
    "longitude": 9.7385532
}, {
    "title": "Berlin",
    "latitude": 52.5170365,
    "longitude": 13.3888599
}, {
    "title": "Kassel",
    "latitude": 51.3154546,
    "longitude": 9.4924096
}, {
    "title": "Köln",
    "latitude": 50.938361,
    "longitude": 6.959974
}, {
    "title": "Aachen",
    "latitude": 50.776351,
    "longitude": 6.083862
}, {
    "title": "Würzburg",
    "latitude": 49.79245,
    "longitude": 9.932966
}, {
    "title": "Frankfurt",
    "latitude": 50.1106444,
    "longitude": 8.6820917
}, {
    "title": "Mainz",
    "latitude": 50.0012314,
    "longitude": 8.2762513
}, {
    "title": "Stuttgart",
    "latitude": 48.7784485,
    "longitude": 9.1800132
}, {
    "title": "München",
    "latitude": 48.1371079,
    "longitude": 11.5753822
}, {
    "title": "Lindau",
    "latitude": 47.550753,
    "longitude": 9.6926624
}
];