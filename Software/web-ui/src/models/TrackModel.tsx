
import IStation from "./StationModel";

export default interface ITrack {
    id?: number;
    start: IStation;
    end: IStation;
    distance: number;
}