
import IUser from "./UserModel";

export default interface IVehicle {
    id?: number;
    licensePlateCode: string;
    isOnRoad?: boolean;
    owner?: string;
    distance?: number;
    totalDistance?: number;
    user?: IUser;
}
