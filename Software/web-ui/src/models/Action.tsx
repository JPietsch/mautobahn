
export default interface IAction {
    id?: number;
    licensePlateCode: string;
    stationId: number;
    timestamp?: string;
}