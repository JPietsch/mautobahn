
import jwt_decode from "jwt-decode";

export const API_URL = "http://localhost:8080/"
export const ACCESS_TOKEN_NAME = "access_token";
export const TOKEN_PREFIX = "Bearer ";
export const AUTH_HEADER = "Authorization";

const LocalStorageService = (function() {

    let _service;

    function _getService() {
        if(!_service) {
            _service = this;
           
        }
        return _service;
    }
    function _setToken(tokenObj) {
        localStorage.setItem(ACCESS_TOKEN_NAME, tokenObj.access_token);
        localStorage.setItem('refresh_token', tokenObj.refresh_token);
    } 
    
    function _getAccessToken() {
        return localStorage.getItem(ACCESS_TOKEN_NAME);
    } 
    
    function _getRefreshToken() {
        return localStorage.getItem('refresh_token');
    } 
    
    function _clearToken() {
        localStorage.removeItem(ACCESS_TOKEN_NAME);
        localStorage.removeItem('refresh_token');
      
    } 
    
    function _getUserRoles() {
        const decToken = jwt_decode(localStorage.getItem(ACCESS_TOKEN_NAME));
        return decToken?.roles;
    }

    function _getUserId() {
        const decToken = jwt_decode(localStorage.getItem(ACCESS_TOKEN_NAME));
        return decToken?.userId;
    }

    return {
        getService : _getService,
        getUserRoles: _getUserRoles,
        getUserId: _getUserId,
        setToken : _setToken,
        getAccessToken : _getAccessToken,
        getRefreshToken : _getRefreshToken,
        clearToken : _clearToken
      }
}) ();

export default LocalStorageService;