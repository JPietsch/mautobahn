
import IStation from "../models/StationModel"

export const Stations:Array<IStation> = [
    {
        "id":1,
        "title":"Bautzen",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude":51.18035,
        "longitude":14.43494
    },
    {
        "id":2,
        "title":"Bautzen",
        "type":"DEPARTURE",
        "distance":63,
        "previous":5,
        "latitude":51.18035,
        "longitude":14.43494
    },
    {
        "id":3,
        "title":"Dresden Nord",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude":51.070406,
        "longitude":13.737263
    },
    {
        "id":4,
        "title":"Dresden Nord",
        "type":"DEPARTURE",
        "distance":193,
        "previous":29,
        "latitude":51.070406,
        "longitude":13.737263
    },
    {
        "id":5,
        "title":"Dresden Südost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude":51.040406,
        "longitude":13.727263
    },
    {
        "id":6,
        "title":"Dresden Südost",
        "type":"DEPARTURE",
        "distance":63,
        "previous":1,
        "latitude":51.040406,
        "longitude":13.727263
    },
    {
        "id":7,
        "title": "Dresden West",
        "type":"PASSAGE",
        "distance":603,
        "previous":null,
        "latitude":51.050407,
        "longitude":13.717261
    },
    {
        "id":8,
        "title": "Dresden West",
        "type":"DEPARTURE",
        "distance":353,
        "previous":33,
        "latitude":51.050407,
        "longitude":13.717261
    },
    {
        "id":9,
        "title": "Hamburg Nord",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude":53.571087,
        "longitude":9.993682
    },
    {
        "id":10,
        "title": "Hamburg Nord",
        "type":"DEPARTURE",
        "distance":165,
        "previous":15,
        "latitude":53.571087,
        "longitude":9.993682
    },
    {
        "id":11,
        "title": "Hamburg Ost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude":53.571087,
        "longitude":10.013682
    },
    {
        "id":12,
        "title": "Hamburg Ost",
        "type":"DEPARTURE",
        "distance":105,
        "previous":17,
        "latitude":53.571087,
        "longitude":10.013682
    },
    {
        "id":13,
        "title": "Hamburg Süd",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude":53.551087,
        "longitude":10.013682
    },
    {
        "id":14,
        "title": "Hamburg Süd",
        "type":"DEPARTURE",
        "distance":170,
        "previous":19,
        "latitude":53.551087,
        "longitude":10.013682
    },
    {
        "id":15,
        "title": "Flensburg",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 54.7833021,
        "longitude": 9.4333264
    },
    {
        "id":16,
        "title": "Flensburg",
        "type":"DEPARTURE",
        "distance":165,
        "previous":9,
        "latitude": 54.7833021,
        "longitude": 9.4333264
    },
    {
        "id":17,
        "title": "Schwerin",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 53.6288297,
        "longitude": 11.4148038
    },
    {
        "id":18,
        "title": "Schwerin",
        "type":"DEPARTURE",
        "distance":105,
        "previous":11,
        "latitude": 53.6288297,
        "longitude": 11.4148038
    },
    {
        "id":19,
        "title": "Hannover Nord",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 52.3744780,
        "longitude": 9.7385532
    },
    {
        "id":20,
        "title": "Hannover Nord",
        "type":"DEPARTURE",
        "distance":170,
        "previous":13,
        "latitude": 52.3744780,
        "longitude": 9.7385532
    },
    {
        "id":21,
        "title": "Hannover Ost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 52.3644780,
        "longitude": 9.7485532
    },
    {
        "id":22,
        "title": "Hannover Ost",
        "type":"DEPARTURE",
        "distance":380,
        "previous":25,
        "latitude": 52.3644780,
        "longitude": 9.7485532
    },
    {
        "id":23,
        "title": "Hannover Süd",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 52.3544780,
        "longitude": 9.7385532
    },
    {
        "id":24,
        "title": "Hannover Süd",
        "type":"DEPARTURE",
        "distance":410,
        "previous":37,
        "latitude": 52.3544780,
        "longitude": 9.7385532
    },
    {
        "id":25,
        "title": "Berlin West",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 52.5170365,
        "longitude": 13.3888598
    },
    {
        "id":26,
        "title": "Berlin West",
        "type":"DEPARTURE",
        "distance":380,
        "previous":21,
        "latitude": 52.5170365,
        "longitude": 13.3888598
    },
    {
        "id":27,
        "title": "Berlin Südwest",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 52.5070365,
        "longitude": 13.3988598
    },
    {
        "id":28,
        "title": "Berlin Südwest",
        "type":"DEPARTURE",
        "distance":360,
        "previous":31,
        "latitude": 52.5070365,
        "longitude": 13.3988598
    },
    {
        "id":29,
        "title": "Berlin Süd",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 52.4970365,
        "longitude": 13.4088598
    },
    {
        "id":30,
        "title": "Berlin Süd",
        "type":"DEPARTURE",
        "distance":193,
        "previous":3,
        "latitude": 52.4970365,
        "longitude": 13.4088598
    },
    {
        "id":31,
        "title": "Kassel Nordost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 51.3154547,
        "longitude": 9.4924097
    },
    {
        "id":32,
        "title": "Kassel Nordost",
        "type":"DEPARTURE",
        "distance":360,
        "previous":27,
        "latitude": 51.3154547,
        "longitude": 9.4924097
    },
    {
        "id":33,
        "title": "Kassel Ost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 51.3054547,
        "longitude": 9.5024097
    },
    {
        "id":34,
        "title": "Kassel Ost",
        "type":"DEPARTURE",
        "distance":353,
        "previous":7,
        "latitude": 51.3054547,
        "longitude": 9.5024097
    },
    {
        "id":35,
        "title": "Kassel Süd",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 51.2954547,
        "longitude": 9.4924097
    },
    {
        "id":36,
        "title": "Kassel Süd",
        "type":"DEPARTURE",
        "distance":180,
        "previous":49,
        "latitude": 51.2954547,
        "longitude": 9.4924097
    },
    {
        "id":37,
        "title": "Köln Nord",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.938362,
        "longitude": 6.959974
    },
    {
        "id":38,
        "title": "Köln Nord",
        "type":"DEPARTURE",
        "distance":410,
        "previous":23,
        "latitude": 50.938362,
        "longitude": 6.959974
    },
    {
        "id":39,
        "title": "Köln West",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.928362,
        "longitude": 6.949974
    },
    {
        "id":40,
        "title": "Köln West",
        "type":"DEPARTURE",
        "distance":110,
        "previous":43,
        "latitude": 50.928362,
        "longitude": 6.949974
    },
    {
        "id":41,
        "title": "Köln Südost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.918362,
        "longitude": 6.969974
    },
    {
        "id":42,
        "title": "Köln Südost",
        "type":"DEPARTURE",
        "distance":230,
        "previous":47,
        "latitude": 50.918362,
        "longitude": 6.969974
    },
    {
        "id":43,
        "title": "Aachen",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.776351,
        "longitude": 6.083862
    },
    {
        "id":44,
        "title": "Aachen",
        "type":"DEPARTURE",
        "distance":110,
        "previous":39,
        "latitude": 50.776351,
        "longitude": 6.083862
    },
    {
        "id":45,
        "title": "Würzburg",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 49.79245,
        "longitude": 9.932966
    },
    {
        "id":46,
        "title": "Würzburg",
        "type":"DEPARTURE",
        "distance":90,
        "previous":67,
        "latitude": 49.79245,
        "longitude": 9.932966
    },
    {
        "id":47,
        "title": "Frankfurt Nordwest",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.1106445,
        "longitude": 8.6820916
    },
    {
        "id":48,
        "title": "Frankfurt Nordwest",
        "type":"DEPARTURE",
        "distance":230,
        "previous":41,
        "latitude": 50.1106445,
        "longitude": 8.6820916
    },
    {
        "id":49,
        "title": "Frankfurt Nord",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.1206445,
        "longitude": 8.6920916
    },
    {
        "id":50,
        "title": "Frankfurt Nord",
        "type":"DEPARTURE",
        "distance":180,
        "previous":35,
        "latitude": 50.1206445,
        "longitude": 8.6920916
    },
    {
        "id":51,
        "title": "Frankfurt Südwest",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.0906445,
        "longitude": 8.6820916
    },
    {
        "id":52,
        "title": "Frankfurt Südwest",
        "type":"DEPARTURE",
        "distance":40,
        "previous":55,
        "latitude": 50.0906445,
        "longitude": 8.6820916
    },
    {
        "id":53,
        "title": "Frankfurt Süd",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.0806445,
        "longitude": 8.6920916
    },
    {
        "id":54,
        "title": "Frankfurt Süd",
        "type":"DEPARTURE",
        "distance":270,
        "previous":59,
        "latitude": 50.0806445,
        "longitude": 8.6920916
    },
    {
        "id":55,
        "title": "Mainz",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.0012314,
        "longitude": 8.2762513
    },
    {
        "id":56,
        "title": "Mainz",
        "type":"DEPARTURE",
        "distance":40,
        "previous":51,
        "latitude": 50.0012314,
        "longitude": 8.2762513
    },
    {
        "id":57,
        "title": "Stuttgart",
        "type":"PASSAGE",
        "distance":270,
        "previous":53,
        "latitude": 48.7784485,
        "longitude": 9.1800132
    },
    {
        "id":58,
        "title": "Stuttgart",
        "type":"DEPARTURE",
        "distance":270,
        "previous":53,
        "latitude": 48.7784485,
        "longitude": 9.1800132
    },
    {
        "id":59,
        "title": "Stuttgart",
        "type":"PASSAGE",
        "distance":160,
        "previous":63,
        "latitude": 48.7784485,
        "longitude": 9.1800132
    },
    {
        "id":60,
        "title": "Stuttgart",
        "type":"DEPARTURE",
        "distance":160,
        "previous":63,
        "latitude": 48.7784485,
        "longitude": 9.1800132
    },
    {
        "id":61,
        "title": "Lindau",
        "type":"PASSAGE",
        "distance":160,
        "previous":57,
        "latitude": 47.550753,
        "longitude": 9.6926624
    },
    {
        "id":62,
        "title": "Lindau",
        "type":"DEPARTURE",
        "distance":160,
        "previous":57,
        "latitude": 47.550753,
        "longitude": 9.6926624
    },
    {
        "id":63,
        "title": "Lindau",
        "type":"PASSAGE",
        "distance":230,
        "previous":65,
        "latitude": 47.550753,
        "longitude": 9.6926624
    },
    {
        "id":64,
        "title": "Lindau",
        "type":"DEPARTURE",
        "distance":230,
        "previous":65,
        "latitude": 47.550753,
        "longitude": 9.6926624
    },
    {
        "id":65,
        "title": "München",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 48.1371079,
        "longitude": 11.5753822
    },
    {
        "id":66,
        "title": "München",
        "type":"DEPARTURE",
        "distance":230,
        "previous":61,
        "latitude": 48.1371079,
        "longitude": 11.5753822
    },
    {
        "id":67,
        "title": "Frankfurt Nordost",
        "type":"PASSAGE",
        "distance":0,
        "previous":null,
        "latitude": 50.1106445,
        "longitude": 8.7020916
    },
    {
        "id":68,
        "title": "Frankfurt Nordost",
        "type":"DEPARTURE",
        "distance":90,
        "previous":45,
        "latitude": 50.1106445,
        "longitude": 8.7020916
    }
]