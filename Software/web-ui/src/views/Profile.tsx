
import { useState, useEffect } from 'react';

import { Button } from '@mui/material';

import axios from 'axios';

import { 
    Paper, Grid
 } from "@mui/material";

import LocalStorageService, { API_URL, ACCESS_TOKEN_NAME, TOKEN_PREFIX } from '../services/LocalStorageService';

import IUser from "../models/UserModel";

export function Profile() {

    const localStorageService = LocalStorageService.getService();

    const[user, setUser] = useState<IUser>()

   
    const fetchUserData = async () => {
        const id = localStorageService.getUserId();
        const result = await axios.get(API_URL + "user/" + id, {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        }
        )
        setUser(result.data);          
    };

    useEffect(() => {
        fetchUserData();
    }, [])

    return(
        <Paper className="Modal" elevation={3}>      
            <Grid container spacing={3} direction="column">
                <Grid item>
                    <h3>Mein Profil</h3>
                </Grid>
                <Grid item container spacing={2} direction="row" justifyContent="space-around">
                    <Grid item>
                        Name:
                    </Grid>
                    <Grid item>
                        {
                            user?.firstName + " " + user?.lastName
                        }
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    );
}

export default Profile;