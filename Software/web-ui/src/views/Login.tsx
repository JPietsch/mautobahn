import { ChangeEvent, useState, useEffect } from 'react';

import axios from "axios";
import jwt_decode from "jwt-decode";

import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';

import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import AccountCircle from '@mui/icons-material/AccountCircle';
import LockIcon from '@mui/icons-material/Lock';
import CloseIcon from '@mui/icons-material/Close';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

import logo from '../assets/Logo.png';

import { API_URL, ACCESS_TOKEN_NAME } from '../services/LocalStorageService';

export interface IToken {
    sub?: string;
    roles?: ['USER', 'ADMIN'];
    iat?: number;
    exp?: number;
}

export function Login({updateLoginState}:{updateLoginState:Function}) {

    const[errorMsg, setErrorMsg] = useState<string>('');
    const[isFirstLogin, setIsFirstLogin] = useState(false);
    const[credentials, setCredentials] = useState({
        username: '',
        password: ''
    });

    const[loginState, setLoginState] = useState<boolean>();

    useEffect(() => {
        updateLoginState(loginState);
    },[updateLoginState, loginState]);

    const onChange = ({target: { name, value}}: ChangeEvent<HTMLInputElement>) => {
        setCredentials({...credentials, [name]: value})
    };

    const onSubmit = () => {
        if(credentials && credentials.username && credentials.password) {
            axios.post(API_URL + 'login', {
                username: credentials.username,
                password: credentials.password
            })
            .then(r => localStorage.setItem(ACCESS_TOKEN_NAME,r.data?.token))
            .then(() => {
                if(isTokenValid()) {
                    setLoginState(true);
                }         
            })
            .catch((e) => {
                alert(e)
                setErrorMsg("Wahrscheinlich ist der Server nicht erreichbar. Bitte versuchen Sie es später nochmal.");
            })
        } else {
            setErrorMsg("Ungültige Login-Daten! Bitte überprüfen Sie Passwort und Benutzername.")
        }
    };

    return (
        <div className="App-header">

        <Paper className="Login" elevation={3}>      
            <Grid container xs={12} spacing={3} direction="column" alignItems="center">
                <Grid item>
                    <img src={logo} alt="Logo"/>
                </Grid>
                <Grid item>
                    <h3>{isFirstLogin && "Erste "}Anmeldung</h3>
                </Grid>           
                    {
                        errorMsg.length > 1 &&
                        <Grid item xs={12} container direction="column" alignItems="center">
                            <Grid item xs={8}>
                                <Alert 
                                severity="error"
                                action={
                                    <IconButton onClick={() => setErrorMsg('')} color="inherit" size="small">
                                        <CloseIcon />
                                    </IconButton>
                                }
                                >
                                    <AlertTitle>Fehler!</AlertTitle>
                                    {errorMsg}
                                </Alert>
                                </Grid>
                        </Grid>
                    }
                
                <Grid item>
                    <UsernameInput onChange={onChange} name="username" label={isFirstLogin ? "Steuer-ID" : "Benutzername"}/>
                </Grid>
                <Grid item>
                    <PasswordInput onChange={onChange} name="password" label={isFirstLogin ? "Initial-Passowort" : "Passwort"}/>
                </Grid>
                <Grid item>
                    <Button variant="contained" onClick={() => setIsFirstLogin(!isFirstLogin)} style={{ marginRight: 3+'em' }}>
                        {isFirstLogin ? "Schon registriert?" : "Erste Anmeldung?" }
                    </Button>
                    <Button onClick={onSubmit} variant="contained" size="large" color={"success"}>
                        Login
                    </Button>
                </Grid>
            </Grid>
        </Paper>
        </div>
    );
}

export const isTokenValid = ():boolean => {
    const token = localStorage.getItem("access_token");
    if(!token) return false;
    if(token.length < 1) return false;

    const decodedToken:IToken = jwt_decode(token);

    if(!decodedToken) return false;
    if(!decodedToken.sub) return false;
    if(!decodedToken.roles) return false;
    if(!decodedToken.exp) return false;
    if(!decodedToken.iat) return false;

    return true;
}

export function UsernameInput({label, name, onChange}:{label:string, name:string, onChange:any}) {

    return(
        <TextField
        onChange={onChange}
        name={name}
        id="input-with-icon-textfield"
        label={label}
        InputProps={{
        startAdornment: (
            <InputAdornment position="start">
            <AccountCircle />
            </InputAdornment>
        ),
        }}
        variant="outlined"
    />
    );
}

export function PasswordInput({label,name, onChange}:{label:string,name:string,onChange:any}) {

    return(
        <TextField
        onChange={onChange}
        name={name}
        id="input-with-icon-textfield"
        type="password"
        label={label}
        InputProps={{
        startAdornment: (
            <InputAdornment position="start">
            <LockIcon />
            </InputAdornment>
        ),
        }}
        variant="outlined"
    />
    );
}