import { useLayoutEffect, useEffect, useRef, useState } from "react";

import axios from "axios";

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import { Paper, Grid} from "@mui/material";

import IStatistic from "../models/Statistic";

import LocalStorageService, { API_URL, ACCESS_TOKEN_NAME, TOKEN_PREFIX } from '../services/LocalStorageService';


export function TrafficStatistic() {
    const localStorageService = LocalStorageService.getService();

    const chart = useRef<any>(null);

    useLayoutEffect(() => {
        am4core.useTheme(am4themes_animated);

        let c = am4core.create("statchartdiv", am4charts.PieChart);
        c.hiddenState.properties.opacity = 0; // this creates initial fade-in

        const fetchData = async () => {
            const result = await axios.get(API_URL + "statistics", {
                headers: {
                    ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                }
            }); 
            c.data = result.data;
        };
        fetchData();

        c.radius = am4core.percent(70);
        c.innerRadius = am4core.percent(40);
        c.startAngle = 180;
        c.endAngle = 360;  

        let series = c.series.push(new am4charts.PieSeries());
        series.dataFields.value = "value";
        series.dataFields.category = "country";

        series.slices.template.cornerRadius = 10;
        series.slices.template.innerCornerRadius = 7;
        series.slices.template.draggable = true;
        series.slices.template.inert = true;
        series.alignLabels = false;

        series.hiddenState.properties.startAngle = 90;
        series.hiddenState.properties.endAngle = 90;

        c.legend = new am4charts.Legend();

        chart.current = c;

        return () => {
            c.dispose()
        }
    }, [])

    return(
        <Paper className="Modal">
            <Grid container spacing={3} direction="column">
                <Grid item>
                    <h3>Verkehrsstatistik</h3>
                </Grid>
                <Grid item>
                    <div id="statchartdiv"/>
                </Grid>
            </Grid>
        </Paper>
    );
}

export default TrafficStatistic;