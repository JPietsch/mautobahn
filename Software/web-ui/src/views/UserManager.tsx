import { useState, useEffect, ChangeEvent } from 'react';

import axios from 'axios';

import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import DescriptionRoundedIcon from '@mui/icons-material/DescriptionRounded';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import PersonIcon from '@mui/icons-material/Person';
import AccessibilityNewIcon from '@mui/icons-material/AccessibilityNew';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';
import EditRoadRoundedIcon from '@mui/icons-material/EditRoadRounded';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import EditLocationAltRoundedIcon from '@mui/icons-material/EditLocationAltRounded';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

import LocalStorageService, { API_URL, ACCESS_TOKEN_NAME, TOKEN_PREFIX } from '../services/LocalStorageService';

import { Paper, Grid, TextField, 
    InputAdornment, Button, IconButton,
    Table, TableBody, TableCell, TableHead,
    TableRow, TableContainer
 } from "@mui/material";
import IUser from '../models/UserModel';

export function UserManager() {
    const[errorMsg, setErrorMsg] = useState<{msg:string; level:'error' | 'success'}>();

    const localStorageService = LocalStorageService.getService();

    const[view, setView] =  useState<'SHOW_ALL' | 'SHOW_ONE' | 'CREATE' | 'DELETE'>('SHOW_ALL');

    const[newUser, setNewUser] = useState(
        {
            "username": "",
            "password": "",
            "firstName": "",
            "lastName": "",
            "roles": [],
            "address.street":"",
            "address.houseNumber":"",
            "address.city.name":"",
            "address.city.postalCode":""
        }
    );

    const[activeUser, setActiveUser] = useState<IUser>();

    const[allUser, setAllUser] = useState<Array<IUser>>([])

    const fetchUsers = async () =>{

        const result = await axios.get(API_URL + "user/all", {
              headers: {
                  ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                  'Access-Control-Allow-Origin': '*',
                  'Content-Type': 'application/json',
              }
          });
          setAllUser(result.data)
      }

    useEffect(() => {
        fetchUsers();
    }, [])

    const createUser = ():IUser => {
        return {
            id: 0,
            username: newUser.username,
            password: newUser.password,
            firstName: newUser.firstName,
            lastName: newUser.lastName,
            address: {
                street: newUser['address.street'],
                houseNumber: newUser['address.houseNumber'],
                city: {
                    name: newUser['address.city.name'],
                    postalCode: newUser['address.city.postalCode']
                }
            }
        }
    }

    const onChange = ({target: { name, value}}: ChangeEvent<HTMLInputElement>) => {
        if(!name) return;
        if(!value) return;
        setNewUser({...newUser, [name]: value })
    };

    const onSave = () => {
        const user = createUser();

        axios.post(API_URL + 'user/save', user)
        .then(resp => {
            if(resp.data) {
                setErrorMsg({msg:"Benutzer wurde erfolgreich gespeichert!", level: "success"})
            } else {
                setErrorMsg({msg:"Bitte überprüfen Sie die Angaben!", level:"error"})
            }
        })
        .catch(e => {
            alert(e)
        })
    }

    const deleteUser = () => {
        axios.get(API_URL + 'user/delete/' + activeUser?.id)
        .then(resp => {
            if(resp.data) {
                setErrorMsg({msg:"Benutzer wurde erfolgreich gelöscht!", level: "success"})
            } else {
                setErrorMsg({msg:"Benutzer konnte nicht gelöscht werden!", level:"error"})
            }
        })
        .catch(e => {
            
        })

    }

    return(
        <Paper className="Modal">
            <Grid container spacing={3} direction="column">
                <Grid item>
                    <h4>
                        {view === 'SHOW_ALL' && 'Benutzer Verwaltung'}
                        {view === 'SHOW_ONE' && 'Benutzer'}
                        {view === 'CREATE' && 'Neuer Benutzer'}
                        {view === 'DELETE' && 'Benutzer löschen'}
                    </h4>
                </Grid>
                {
                        errorMsg &&
                        <Grid item xs={12} container direction="column" alignItems="center">
                            <Grid item xs={8}>
                                <Alert 
                                severity={errorMsg.level}
                                action={
                                    <IconButton onClick={() => setErrorMsg(undefined)} color="inherit" size="small">
                                        <CloseIcon />
                                    </IconButton>
                                }
                                >
                                    <AlertTitle>{errorMsg.level === 'error' ? "Fehler!" : "Erfolgreich!"}</AlertTitle>
                                    {errorMsg.msg}
                                </Alert>
                                </Grid>
                        </Grid>
                    }
                { view === 'SHOW_ALL' &&
                <>
                <Grid item>
                    <TextField
                        id="input-with-icon-textfield"
                        type="text"
                        label={"Suche"}
                        placeholder={"Nach Nutzer suchen"}
                        InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <IconButton>
                                    <SearchRoundedIcon />
                                </IconButton>
                            </InputAdornment>
                        ),
                        }}
                        variant="outlined"
                    />
                </Grid>
                <Grid item>
                    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer  sx={{ maxHeight: 450 }}>
                        <Table stickyHeader>
                            <TableHead>
                                <TableRow>
                                    <TableCell><strong>Vorname</strong></TableCell>
                                    <TableCell><strong>Nachname</strong></TableCell>
                                    <TableCell><strong>Wohnort</strong></TableCell>
                                    <TableCell align="right"><strong>Anzeigen</strong></TableCell>
                                    <TableCell align="right"><strong>Löschen</strong></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    allUser.map((user) => (
                                        <TableRow
                                            key={user.id}
                                        >
                                            <TableCell>{user.firstName ? user.firstName : user.username}</TableCell>
                                            <TableCell>{user.lastName}</TableCell>
                                            <TableCell>{user.address?.city.name}</TableCell>
                                            <TableCell align="right">
                                                <IconButton onClick={() => {
                                                    setView('SHOW_ONE')
                                                    setActiveUser(user)
                                                }}>
                                                    <DescriptionRoundedIcon color="primary"/>
                                                </IconButton>
                                            </TableCell>
                                            <TableCell align="right">
                                                <IconButton onClick={() => {
                                                    setView('DELETE')
                                                    setActiveUser(user)
                                                }}>
                                                    <DeleteForeverIcon color="error"/>
                                                </IconButton>
                                            </TableCell> 
                                        </TableRow>
                                        
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    </Paper>
                </Grid>
                <Grid item>
                    <Button onClick={() => setView('CREATE')} variant="contained" color="success" startIcon={<AddRoundedIcon />}>
                        Benutzer hinzufügen
                    </Button>
                </Grid>
                </>
                }
                {
                    view === 'CREATE' &&
                    <>
                    <Grid item container spacing={2} direction="row">
                            <Grid item xs={6}>
                                <TextField
                                    onChange={onChange}
                                    name="username"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"Benutzername"}
                                    placeholder={"user123"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <PersonIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onChange={onChange}
                                    name="password"
                                    id="input-with-icon-textfield"
                                    type="password"
                                    label={"Initial-Password"}
                                    placeholder={"Sup3rS3creTp4ssW0rt"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <AccessibilityNewIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item container spacing={2} direction="row">
                            <Grid item xs={6}>
                                <TextField
                                    onChange={onChange}
                                    name="firstName"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"Vorname"}
                                    placeholder={"Max"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <AccountCircleRoundedIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onChange={onChange}
                                    name="lastName"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"Nachname"}
                                    placeholder={"Musterman"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <AccountCircleRoundedIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} container spacing={2} direction="row" justifyContent="flex-start">
                            <Grid item xs={6}>
                                <TextField
                                    onChange={onChange}
                                    name="address.street"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"Straße"}
                                    placeholder={"Musterstraße"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <EditRoadRoundedIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <TextField
                                    onChange={onChange}
                                    name="address.houseNumber"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"Hausnummer"}
                                    placeholder={"1a"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <HomeRoundedIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item container direction="row">
                            <Grid item xs={4}>
                                <TextField
                                    onChange={onChange}
                                    name="address.city.postalCode"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"PLZ"}
                                    placeholder={"012345"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <SearchRoundedIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    onChange={onChange}
                                    name="address.city.name"
                                    id="input-with-icon-textfield"
                                    type="text"
                                    label={"Ort"}
                                    placeholder={"Musterstadt"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <EditLocationAltRoundedIcon />
                                        </InputAdornment>
                                    ),
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} container spacing={2} direction="row" justifyContent="center">
                            <Grid item>
                                <Button onClick={onSave} variant="contained" size="large" color={"success"}>
                                    Speichern
                                </Button>
                            </Grid>
                        </Grid>
                        </>
                }
                {
                    view === 'DELETE' &&
                        <Grid item xs={12} spacing={2} container direction="column" alignItems="center">
                            <Grid item>
                                {`Soll der Benutzer "${activeUser?.firstName} ${activeUser?.lastName}" wirklich dauerhaft gelöscht werden?`}
                            </Grid>
                            <Grid item>
                                <Button onClick={deleteUser} color="error" variant="outlined" startIcon={<DeleteIcon />}>
                                    Löschen
                                </Button>
                            </Grid>
                        </Grid>
                }
                {
                    view === 'SHOW_ONE' &&
                    <div style={{ fontSize: 1.5+'rem' }}>
                        <Grid item  spacing={2} container direction="row" justifyContent="flex-start">
                            <Grid item xs={6}>
                               <strong>Vorname:</strong>
                            </Grid>
                            <Grid item>
                                {activeUser?.firstName}
                            </Grid>
                        </Grid>
                        <Grid item  spacing={2} container direction="row" justifyContent="flex-start">
                            <Grid item xs={6}>
                               <strong>Nachname:</strong>
                            </Grid>
                            <Grid item>
                                {activeUser?.lastName}
                            </Grid>
                        </Grid>
                        <Grid item  spacing={2} container direction="row" justifyContent="flex-start">
                            <Grid item xs={6}>
                               <strong>Benutzername:</strong>
                            </Grid>
                            <Grid item>
                                {activeUser?.username}
                            </Grid>
                        </Grid>
                        <Grid item  spacing={2} container direction="row" justifyContent="flex-start">
                            <Grid item xs={6}>
                               <strong>Addresse:</strong>
                            </Grid>
                            <Grid item>
                                {`${activeUser?.address?.street} ${activeUser?.address?.houseNumber}`}
                            </Grid>
                            <Grid item xs={6}>
                               <strong>Ort:</strong>
                            </Grid>
                            <Grid item>
                                {`${activeUser?.address?.city.postalCode} ${activeUser?.address?.city.name}`}
                            </Grid>
                        </Grid>
                    </div>
                }
                {
                    view !== 'SHOW_ALL' &&
                    <Grid item xs={12} container spacing={2} direction="row" justifyContent="center">
                        <Grid item>
                            <Button  variant="contained" onClick={() => {
                                setView('SHOW_ALL')
                                setErrorMsg(undefined)
                                fetchUsers();
                            }}>
                                Zurück
                            </Button>
                        </Grid>
                    </Grid>
                }
            </Grid>
        </Paper>
    );
}

export default UserManager;