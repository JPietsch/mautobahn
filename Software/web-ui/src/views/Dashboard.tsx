
import { useState, useEffect, useCallback } from 'react';

import SideMenu from './SideMenu';
import Header from './Header';
import Bill from './Bill';

function Dashboard({updateLoginState}:{updateLoginState:Function}) {

    const[viewComponent, setViewComponent] = useState<React.ReactElement>(<Bill/>);

    const updateView = useCallback(val => {
      setViewComponent(val);
    }, [setViewComponent]);

    const[loginState, setLoginState] = useState<boolean>(true);

    useEffect(() => {
        updateLoginState(loginState);
    },[updateLoginState, loginState]);

    return (
        <>
              <Header updateLoginState={updateLoginState}/>
              <SideMenu updateView={updateView} />
              
              <div className="App-header">
                {viewComponent}
              </div>
        </>
    )    
}

export default Dashboard;