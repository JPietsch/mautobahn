
import Button from '@mui/material/Button';
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded';

export function Header({updateLoginState}:{updateLoginState:Function}) {

    const logout = () => {
        localStorage.setItem("access_token", "");
        updateLoginState(false);
    }

    return (
        <div className="Header">
            <div className="Brand">
                Mautobahn
            </div>

            <div className="Logout">
                <Button onClick={logout} variant="outlined">
                    <LogoutRoundedIcon fontSize="large"/>
                    Logout
                </Button>
            </div>
        </div>
    );
}

export default Header;