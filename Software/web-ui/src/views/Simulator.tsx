
import { useState, useEffect, ChangeEvent } from "react";

import axios from "axios";
import faker from "faker";

import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/material/styles';

import CheckRoundedIcon from '@mui/icons-material/CheckRounded';
import EditRoundedIcon from '@mui/icons-material/EditRounded';
import PlayCircleFilledWhiteRoundedIcon from '@mui/icons-material/PlayCircleFilledWhiteRounded';
import AddCircleOutlineRoundedIcon from '@mui/icons-material/AddCircleOutlineRounded';
import CheckCircleOutlineRoundedIcon from '@mui/icons-material/CheckCircleOutlineRounded';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';

import { CircularProgress, InputLabel, Select, MenuItem, Modal, Paper, Grid, TextField, 
    InputAdornment, Button, Chip, Input, IconButton, Stack } from "@mui/material";

import IUser from "../models/UserModel";
import IVehicle from "../models/VehicleModel";
import IStation from "../models/StationModel";
import IStatistic from "../models/Statistic";
import ITrack from "../models/TrackModel";
import IAdress from "../models/AddressModel";
import IAction from "../models/Action";
import ITask, { ITestCase, TestCaseTemplate, TaskTemplate, TestCase, Pfahrten } from "../models/TaskModel";

import LocalStorageService, { API_URL, ACCESS_TOKEN_NAME, TOKEN_PREFIX } from '../services/LocalStorageService';

import { CountryCodes } from "../assets/CountryCodes";
import { ILocation } from "../models/TaskModel";
import { locations } from "../models/TaskModel";
import { useScrollTrigger } from "@material-ui/core";

import { Stations } from "../assets/staions";

export function Simulator() {
    const localStorageService = LocalStorageService.getService();

    const[isLoading, setIsLoading] = useState(false);

    const[testCases,setTestCases] = useState<Array<ITestCase>>([{...TestCaseTemplate}])
    const[currentTestCase,setCurrentTestCase] = useState<ITestCase>(testCases[0]);
    const[currentTask, setCurrentTask] = useState<ITask>({...TaskTemplate});

    const[vehicles, setVehicles] = useState<Array<IVehicle>>([])
    const[log, setLog] = useState<Array<string>>([]);
    const[status, setStatuse] = useState< 'success' | 'failed'>();

    const[modalOpen, setModalOpen] = useState<boolean>(false);
    const[taskType, setTaskType] = useState<'REGUREMENT' | 'TEST' | 'RESULT'>('REGUREMENT');

    let tabIndex = testCases && currentTestCase ? testCases.indexOf(currentTestCase) : 0;

    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      }));

    const getData = async (url:string) => {
        const result = await axios.get(url, {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        });
        //alert(JSON.stringify(result.data))
        return result.data;         
    };

    const postData = async (url:string, object:any) => {
        const result = await axios.post(url, object)
        return result.data;
    };


    function a11yProps(index: number) {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }      

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        if(!testCases) return;
        setCurrentTestCase(testCases[newValue]);
    };

    const applyCurrentTask = () => {
       // alert(JSON.stringify(currentTestCase))

        if(currentTask.title === TaskTemplate.title) return;
        
        if(taskType === 'REGUREMENT') {
            currentTestCase.reqiurements.push(currentTask);
        }
        if(taskType === 'TEST') {
            currentTestCase.tests.push(currentTask);
        }
        if(taskType === 'RESULT') {
            currentTestCase.results.push(currentTask);
        }
        
        setModalOpen(false)
    }

    const applyTestCase = () => {
        if(!currentTestCase) return;
        testCases.push(currentTestCase);
        setTestCases(testCases)
    }

    const letters = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z"
    ]

    const randNumber = (min:number, max:number):number => {
        const num:number = Math.random() * (max - min) + min
        return Math.trunc(num);
    }
    
    const randLetter = () => {
        return letters[randNumber(0,letters.length-1)]
    }

    function randCountryCode() {
        const key = Object.keys(CountryCodes)[randNumber(0,Object.keys(CountryCodes).length-1)]
        return key
    }

    const randomAddress = ():IAdress => {
        return {
            street: faker.address.streetName(),
            houseNumber: ''+faker.datatype.number(),
            city: {
                name: faker.address.cityName(),
                postalCode: faker.address.zipCode(),
            }
        }
    }

    const randomLPC = () => {
        let lpc = "";
        lpc += randCountryCode()
        lpc += '-'+randLetter()+randLetter();
        lpc += '-'+randLetter()+randLetter();
        lpc += randNumber(10,900)
        return lpc;
    }

    const randomUser = ():IUser => {
        const user:IUser = {
            id: 0,
            email: faker.internet.email(),
            username: faker.internet.userName(),
            password: faker.internet.password(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            address: randomAddress(),
            roles: ['USER']
        }
        return user;
    }

    const randomStation = ():IStation => {
        let loc = locations[randNumber(0,locations.length-1)]
        alert(JSON.stringify(loc))
        return {
            title: loc.title,
            longitude: ''+loc.longitude,
            latitude: ''+loc.latitude,
            distance: ''+randNumber(10, 100),
            type: 'PASSAGE'   
        };
 
    }


    const executeRequest = async (type : 'GET' | 'POST', url:string, obj: IUser | IStation | IVehicle | undefined) => {
        

        if(type === 'GET') {
            const result = await axios.get(API_URL + url, {
                headers: {
                    ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                }
            })
            return result.data;    
        }

        if(type === 'POST') {
            const result = await axios.post(API_URL + url, obj, {
                headers: {
                    ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                }
            });
            
        }
       
    }


    const fetchVehicles = async () => {
        const result = await  axios.get(API_URL + "vehicle/all", {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        })
      setVehicles(result.data);       
    };

    useEffect(() => {

        fetchVehicles()

    },[])

    const createUserAndVehicle = () => {
        setIsLoading(true)

            axios.post(API_URL + 'station/saveAll', Stations)
            .catch(e => {
                alert(e)
            })

            const l = [...log]
            l.push(`${Stations.length} Stationen wurden gespeichert`)
            setLog(l)
            
        const amount = 3;

            for(let i = 1; i <= amount; i++) {
                let user:IUser = randomUser();

                const vehicle:IVehicle = {
                    id: randNumber(0,amount),
                    licensePlateCode: randomLPC(),
                    isOnRoad: false,
                    owner: ''+user.id,
                    user: user
                }

                axios.post(API_URL + "vehicle/save", vehicle, {
                    headers: {
                        ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json',
                    }
                })
            }

            l.push(`Es wurden ${amount} Fahrzeuge mit Besitzern erstellt`)
            setLog(l)
  
            setIsLoading(false)
    }

    const simulateTracks = () => {
        const t = Pfahrten[randNumber(0,Pfahrten.length-1)]

        fetchVehicles();


        const l = [...log]
        
        vehicles.forEach((v,i) => {
            setTimeout(function() {
                t.forEach((s,j) => {
                
                    setTimeout(function() {

                        let date = new Date();
                        // var dateString = `${m.getUTCFullYear()}-${m.getUTCMonth() < 10 ? '0' : ''}${m.getUTCMonth()+1}-${m.getUTCDate() < 10 ? '0' : ''}${m.getUTCDate()}T${m.getUTCHours()}:${m.getUTCMinutes()}:${m.getUTCSeconds()}`;
                
                            let result = date.toISOString().split('T')[0];
                            let result1 = (date.toISOString().split('T')[1]).slice(0,-5);

                            const action = {
                                licensePlateCode: v.licensePlateCode,
                                stationId: s,
                                timestamp: result + "T"+ result1
                            }

                            axios.post(API_URL + "action/update", action, {
                                headers: {
                                    ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                                    'Access-Control-Allow-Origin': '*',
                                    'Content-Type': 'application/json',
                                }
                            })
                            .catch(error => alert(error))


                            
                    }, j * 1000);
                    
                })

            }, i * 1000);
        })

       
        l.push(`${vehicles.length} Fahrzeuge fahren jeweils ${t.length} Strecken`)
        setLog(l)
    }

    return(
        <Paper className="Modal">
            <Grid container spacing={3} direction="column">
                <Grid item>
                    <h4>Simulator</h4>
                </Grid>
                <Grid item>
                    {
                       isLoading ? 
                       <CircularProgress style={{ fontSize: 20+'rem' }}/> 
                       : 
                       <>
                       <CheckCircleOutlineRoundedIcon style={{ fontSize: 10+'rem' }} fontSize="large" color="success"/>                        
                       </>
                    }
                </Grid>
                <Grid item>
                    { log && log.length > 0 &&
                    <Stack spacing={2}>
                        {
                            log.map((l) =>
                             <Item color={status !== 'success' ? 'error' : 'success'}>{l}</Item>
                            )
                        }
                    </Stack> 
                    }

                </Grid>
                <Grid item>
                    <Button onClick={createUserAndVehicle} color="primary" variant="contained" startIcon={<PlayCircleFilledWhiteRoundedIcon/>}>
                        Generiere Daten
                    </Button>
                </Grid>
                <Grid item>
                    <Button onClick={simulateTracks} color="success" variant="contained" startIcon={<PlayCircleFilledWhiteRoundedIcon/>}>
                        Start!
                    </Button>
                </Grid>
            </Grid>
        </Paper>
    );
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
  }
  
  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

export default Simulator;


/**

                     <Stack direction="row" spacing={1}>
                        <Chip
                        label="User"
                        variant="filled"
                        color="primary"
                        onDelete={() => {}}
                        />
                    </Stack>
 
                <Grid item>
                    <Box sx={{ width: '100%' }}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <Tabs value={tabIndex} onChange={handleChange} aria-label="basic tabs example">
                                {
                                    testCases.map((tc, index) => 
                                        <Tab label={tc.title} {...a11yProps(index)} />
                                    )
                                }
                            </Tabs>
                        </Box>
                            {
                                testCases.map((tc, index) => 
                                <TabPanel value={tabIndex} index={index}>
                                    <Grid container spacing={2} direction="column" justifyContent="center">
                                        <Grid item>
                                            {
                                                tc.title !== TestCaseTemplate.title ?
                                                <strong>{tc.title}</strong>
                                                :
                                                <TextField
                                                    onChange={(e) => {
                                                        currentTestCase.title = e.target.value
                                                    }}
                                                    name="title"
                                                    id="input-with-icon-textfield"
                                                    label={"Bezeichnung"}
                                                    placeholder={TestCaseTemplate.title}
                                                    InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                            <EditRoundedIcon />
                                                        </InputAdornment>
                                                    ),
                                                    }}
                                                    variant="outlined"
                                                />
                                            }            
                                        </Grid>
                                        <Grid item>
                                            {
                                               tc.description !== TestCaseTemplate.description ?
                                                tc.description
                                                :
                                                <TextField
                                                    onChange={(e) => {
                                                        currentTestCase.description = e.target.value
                                                    }}
                                                    name="description"
                                                    id="input-with-icon-textfield"
                                                    label={"Beschreibung"}
                                                    placeholder={TestCaseTemplate.description}
                                                    multiline
                                                    rows={3}
                                                    variant="outlined"
                                                    style={{ minWidth: 100+'%' }}
                                                />
                                            }
                                        </Grid>
                                        <Grid item>
                                            <strong>Anforderungen</strong>
                                        </Grid>
                                        <Grid item >
                                            <Stack spacing={2}>{
                                                tc.reqiurements.map(t => 
                                                    <Item>
                                                        {t.title}
                                                    </Item>
                                                )
                                                }
                                            </Stack>
                                                <Button onClick={() => {
                                                    setModalOpen(true)
                                                    setTaskType('REGUREMENT')
                                                }} color={"info"} variant="outlined" 
                                                endIcon={<AddCircleOutlineRoundedIcon />}
                                                style={{ marginTop: 1+'em' }}
                                                >
                                                    Anforderung erstellen 
                                                </Button>
                                        </Grid>

                                    </Grid>
                                </TabPanel>
                                )
                            }
                    </Box>
                </Grid>
                <Grid item>
                {
                    TestCaseTemplate.title !== currentTestCase.title ?
                     <Button color="success" variant="contained" startIcon={<PlayCircleFilledWhiteRoundedIcon/>}>
                        Start!
                    </Button>
                    :
                    <Button onClick={applyTestCase} color="success" variant="contained" startIcon={<CheckRoundedIcon/>}>
                        Speichern
                    </Button>
                }
                </Grid> 

                <Modal
                open={modalOpen}
            >
             <div className="App-header">
            <Paper className="Login">
                <Grid container spacing={2} direction="column" justifyContent="center">
                    <Grid item>
                        <strong>
                            {`Aufgabe als
                            ${taskType === 'REGUREMENT' ? "Anforderung" : ''}
                            ${taskType === 'TEST' ? "Test" : ''}
                            ${taskType === 'RESULT' ? "Prüfung" : ''}
                            erstellen`}
                        </strong>
                    </Grid>
                <Grid item container spacing={1} direction="row">
                    <Grid item>
                            <TextField
                                onChange={(e) => {
                                    currentTask.title = e.target.value;
                                }}
                                name="title"
                                id="input-with-icon-textfield"
                                label={"Bezeichnung"}
                                placeholder={TestCaseTemplate.title}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <EditRoundedIcon />
                                    </InputAdornment>
                                ),
                                }}
                                variant="outlined"
                            />                             
                    </Grid>
                    <Grid item>
                            <TextField
                                onChange={(e) => {
                                    currentTask.description = e.target.value;
                                }}
                                name="description"
                                id="input-with-icon-textfield"
                                label={"Beschreibung"}
                                placeholder={TestCaseTemplate.description}
                                multiline
                                rows={3}
                                variant="outlined"
                                style={{ width: 100+'%' }}
                            />
                    </Grid>
                </Grid>
                <Grid item container xs={12} spacing={2} direction="row">
                    <Grid item xs={2}>
                        <InputLabel id="demo-simple-select-label">Action</InputLabel>
                            <Select
                                onChange={(e) => setCurrentTask({...currentTask, ["requestType"]: e.target.value as typeof currentTask.requestType})}
                                name="requestType"
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                sx={{ minWidth: '9ch' }}
                            >
                                <MenuItem value={'GET'}>GET</MenuItem>
                                <MenuItem value={'POST'}>POST</MenuItem>
                            </Select>
                    </Grid>
                    <Grid item xs={10}>
                        <InputLabel id="demo-simple-select-label">Request-URL</InputLabel>
                        <TextField
                        onChange={(e) => {
                            currentTask.url = e.target.value;
                        }}
                        id="outlined-start-adornment"
                        InputProps={{
                            startAdornment: <InputAdornment position="start">{API_URL}</InputAdornment>,
                        }}
                        sx={{ width: '100%' }}
                        />
                    </Grid>
                </Grid>
                {
                    currentTask.requestType === "POST" &&
                    <Grid item container xs={12} spacing={2} direction="row">

                    <Grid item>
                        <InputLabel id="demo-simple-select-label">Objkt-Typ</InputLabel>
                        <Select
                            onChange={(e) => setCurrentTask({...currentTask, ["objectType"]: e.target.value as typeof currentTask.objectType})}
                            name="object"
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            sx={{ minWidth: '10ch' }}
                        >
                            <MenuItem value={'User'}>User</MenuItem>
                            <MenuItem value={'Vehicle'}>Vehicle</MenuItem>
                            <MenuItem value={'Station'}>Station</MenuItem>
                        </Select>
                    </Grid>

                    <Grid item>
                        <InputLabel id="demo-simple-select-label">Anzahl</InputLabel>
                        <TextField
                        onChange={(e) => setCurrentTask({...currentTask, ["objectAmount"]: e.target.value as typeof currentTask.objectAmount})}
                        type="number"
                        id="outlined-start-adornment"
                        sx={{ width: '4em' }}
                        />
                    </Grid>
                    </Grid>
                }
                <Grid item>
                    <Button onClick={() => setModalOpen(false)} variant="contained" color="error" style={{ marginRight: 4+'em' }}>
                        Abbrechen
                    </Button>
                    <Button onClick={applyCurrentTask} variant="contained" color="success" style={{ marginLeft: 4+'em' }}>
                        Speichern
                    </Button>
                </Grid>
                </Grid>
            </Paper> 
            </div>
            </Modal>
       
 */