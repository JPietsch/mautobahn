
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import FileDownloadRoundedIcon from '@mui/icons-material/FileDownloadRounded';
import DescriptionRoundedIcon from '@mui/icons-material/DescriptionRounded';

import { Paper, Grid, TextField, 
    InputAdornment, IconButton,
    Table, TableBody, TableCell, TableHead,
    TableRow, TableContainer
 } from "@mui/material";

export function Bill() {

    const billData = [
        {
            id: 3627383,
            month: 'Januar',
            distance: 345.67,
            cost: 34.58
        },
        {
            id: 5444,
            month: 'Februar',
            distance: 34.97,
            cost: 2.45
        },
        {
            id: 3627383,
            month: 'Januar',
            distance: 345.67,
            cost: 34.58
        },
        {
            id: 756477763,
            month: 'Februar',
            distance: 34.97,
            cost: 2.45
        },
        {
            id: 362227383,
            month: 'Januar',
            distance: 345.67,
            cost: 34.58
        },
        {
            id: 7569463,
            month: 'Februar',
            distance: 34.97,
            cost: 2.45
        },
        {
            id: 36222738,
            month: 'Januar',
            distance: 345.67,
            cost: 34.58
        },
        {
            id: 756943,
            month: 'Februar',
            distance: 34.97,
            cost: 2.45
        }
    ];

    return(
        <Paper className="Modal">
            <Grid container spacing={3} direction="column">
                <Grid item>
                    <h3>Meine Rechnungen</h3>
                </Grid>
                <Grid item>
                    <TextField
                        id="input-with-icon-textfield"
                        type="text"
                        label={"Suche"}
                        placeholder={"Nach Rechnung suchen"}
                        InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <IconButton>
                                    <SearchRoundedIcon />
                                </IconButton>
                            </InputAdornment>
                        ),
                        }}
                        variant="outlined"
                    />
                </Grid>
                <Grid item>
                    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer  sx={{ maxHeight: 450 }}>
                        <Table stickyHeader>
                            <TableHead>
                                <TableRow>
                                    <TableCell><strong>Monat</strong></TableCell>
                                    <TableCell><strong>Gefahrene km</strong></TableCell>
                                    <TableCell><strong>Gebühren</strong></TableCell>
                                    <TableCell align="right"><strong>Anzeigen</strong></TableCell>
                                    <TableCell align="right"><strong>Download</strong></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    billData.map((bill) => (
                                        <TableRow
                                            key={bill.id}
                                            
                                        >
                                           
                                            <TableCell>{bill.month}</TableCell>
                                            <TableCell>{`${bill.distance} km`}</TableCell>
                                            <TableCell>{`${bill.cost} €`}</TableCell>
                                            <TableCell align="right">
                                                <IconButton>
                                                    <DescriptionRoundedIcon color="primary"/>
                                                </IconButton>
                                            </TableCell>
                                            <TableCell align="right">
                                                <IconButton>
                                                    <FileDownloadRoundedIcon color="warning"/>
                                                </IconButton>
                                            </TableCell>
                                            
                                        </TableRow>
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    </Paper>
                </Grid>
            </Grid>
        </Paper>
    );
}

export default Bill;