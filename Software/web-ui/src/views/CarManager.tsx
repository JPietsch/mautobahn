import { useState, useEffect } from 'react';

import axios from 'axios';

import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import DescriptionRoundedIcon from '@mui/icons-material/DescriptionRounded';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';

import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

import { Paper, Grid, TextField, 
    InputAdornment, Button, IconButton,
    Table, TableBody, TableCell, TableHead,
    TableRow, TableContainer, InputLabel, Select, MenuItem
 } from "@mui/material";

 import LocalStorageService, { API_URL, ACCESS_TOKEN_NAME, TOKEN_PREFIX } from '../services/LocalStorageService';

 import IUser from '../models/UserModel';
 import IVehicle from '../models/VehicleModel';

export function CarManager() {
    const localStorageService = LocalStorageService.getService();
    const[errorMsg, setErrorMsg] = useState<{msg:string; level:'error' | 'success'}>();

    const[view, setView] =  useState<'SHOW_ALL' | 'SHOW_ONE' | 'CREATE' | 'DELETE'>('SHOW_ALL');

    const[user,setUser] = useState<Array<IUser>>();
    const[cars,setCars] = useState<Array<IVehicle>>();
    const[activeCar, setActiveCar] = useState<IVehicle>();


    const saveCar:IVehicle = {
        licensePlateCode: "",
        isOnRoad: false,
        owner: "0"
    }


    const fetchUserData = async () => {
        const result = await axios.get(API_URL + "user/all", {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        });
        setUser(result.data);          
    };

    const fetchCarsData = async () => {
        const result = await axios.get(API_URL + "vehicle/all", {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        });
        //alert(JSON.stringify(result.data))
        setCars(result.data);          
    };

    useEffect(() => {
        fetchCarsData();
        fetchUserData();
    }, [])

    const updateActiveUserById = (id:string):IUser | undefined => {
        axios.get(API_URL + "user/"+id, {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        })
        .then(res => { 
           return res.data; 
        });
        
        return undefined;
    }

    const onSave = () => {
        axios.post(API_URL + 'vehicle/save', saveCar)
        .then(resp => {
            if(resp.data) {
                setErrorMsg({msg:"Fahrzeug wurde erfolgreich gespeichert!", level: "success"})
            } else {
                setErrorMsg({msg:"Bitte überprüfen Sie die Angaben!", level:"error"})
            }
        })
        .catch(e => {
            alert(e)
        })
    }

    const deleteCar = () => {
        axios.get(API_URL + 'vehicle/delete/' + activeCar?.licensePlateCode)
        .then(resp => {
            if(resp.data) {
                setErrorMsg({msg:"Fahrzeug wurde erfolgreich gelöscht!", level: "success"})
            } else {
                setErrorMsg({msg:"Fahrzeug konnte nicht gelöscht werden!", level:"error"})
            }
        })
        .catch(e => {
            
        })

    }

    return(
        <Paper className="Modal">
            <Grid container spacing={3} direction="column">
                <Grid item>
                    <h4>
                        {view === 'SHOW_ALL' && 'Fahrzeug Verwaltung'}
                        {view === 'SHOW_ONE' && 'Fahrzeug'}
                        {view === 'CREATE' && 'Neues Fahrzeug'}
                        {view === 'DELETE' && 'Fahrzeug löschen'}
                    </h4>
                </Grid>
                {
                        errorMsg &&
                        <Grid item xs={12} container direction="column" alignItems="center">
                            <Grid item xs={8}>
                                <Alert 
                                severity={errorMsg.level}
                                action={
                                    <IconButton onClick={() => setErrorMsg(undefined)} color="inherit" size="small">
                                        <CloseIcon />
                                    </IconButton>
                                }
                                >
                                    <AlertTitle>{errorMsg.level === 'error' ? "Fehler!" : "Erfolgreich!"}</AlertTitle>
                                    {errorMsg.msg}
                                </Alert>
                                </Grid>
                        </Grid>
                    }
                { view === 'SHOW_ALL' &&
                <>
                <Grid item>
                    <TextField
                        id="input-with-icon-textfield"
                        type="text"
                        label={"Suche"}
                        placeholder={"Nach Nutzer suchen"}
                        InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <IconButton>
                                    <SearchRoundedIcon />
                                </IconButton>
                            </InputAdornment>
                        ),
                        }}
                        variant="outlined"
                    />
                </Grid>
                
                {(cars && cars.length > 0)  ?  
                <Grid item>
                    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer  sx={{ maxHeight: 450 }}>
                        <Table stickyHeader>
                            <TableHead>
                                <TableRow>
                                    <TableCell><strong>Kennzeichen</strong></TableCell>
                                    <TableCell><strong>Besitzer</strong></TableCell>
                                    <TableCell><strong>Gefahrene km</strong></TableCell>
                                    <TableCell><strong>Gesamtstrecke</strong></TableCell>
                                   
                                    <TableCell align="right"><strong>Löschen</strong></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                   cars && cars.map((car) => 
                                                          
                                        <TableRow
                                            key={car.id}
                                        > 
                                            <TableCell>{car.licensePlateCode}</TableCell>
                                            <TableCell>{car.user ? 
                                                (car.user.firstName ? 
                                                    (car.user.firstName + " " + car.user.lastName) : car.user?.username)
                                                : "Nicht gefunden"}</TableCell>
                                            <TableCell>{car.distance ? car.distance : 0}</TableCell>
                                            <TableCell>{car.totalDistance ? car.totalDistance : 0}</TableCell>
 
                                            <TableCell align="right">
                                                <IconButton onClick={() => {
                                                    setView('DELETE')
                                                    setActiveCar(car)
                                                }}>
                                                    <DeleteForeverIcon color="error"/>
                                                </IconButton>
                                            </TableCell> 
                                        </TableRow>       
                                   
                                    )
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    </Paper>
                </Grid>
                
                :
                
                    <Grid item>
                        <h4>Keine Fahrzeuge in der Datenbank!</h4>
                    </Grid>
                }
                <Grid item>
                    <Button onClick={() => setView('CREATE')} variant="contained" color="success" startIcon={<AddRoundedIcon />}>
                        Fahrzeug hinzufügen
                    </Button>
                </Grid>
                </>
                }
                {
                    view === 'CREATE' &&
                    <>
                        <Grid item container spacing={2} direction="row">
                        <Grid item xs={6}>
                            <InputLabel id="demo-simple-select-label">Eigentümer</InputLabel>
                                <Select
                                    onChange={(e) => {
                                    e.target.value && (saveCar.owner = e.target.value as string);
                                    }}
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    style={{ width: 15+'em' }}
                                >
                                    {
                                        user && user.map((user) => 
                                            <MenuItem value={user.id}>{ user.firstName ? (user.firstName+" "+user.lastName) : user.username}</MenuItem>
                                        )
                                    }
                                </Select>
                            </Grid>
                            <Grid item xs={6}>
                                <InputLabel id="demo-simple-text-label">Kennzeichen</InputLabel>
                                <TextField
                                    onChange={(e) => saveCar.licensePlateCode = e.target.value}
                                    id="input-with-icon-textfield"
                                    type="text"
                                    placeholder={"DE XX-YY-ZZZ"}
                                    InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                                <SearchRoundedIcon />
                                        </InputAdornment>
                                    )
                                    }}
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={12} container spacing={2} direction="row" justifyContent="center">
                                <Grid item>
                                    <Button onClick={onSave} variant="contained" size="large" color={"success"}>
                                        Speichern
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </>
                }
                            {
                    view === 'DELETE' &&
                        <Grid item xs={12} spacing={2} container direction="column" alignItems="center">
                            <Grid item>
                                {`Soll das Fahrzeug "${activeCar?.licensePlateCode}" von "
                                    ${activeCar?.user?.firstName ? activeCar?.user?.firstName +" "+ activeCar?.user?.lastName : activeCar?.user?.username }" wirklich dauerhaft gelöscht werden?`}
                            </Grid>
                            <Grid item>
                                <Button onClick={deleteCar} color="error" variant="outlined" startIcon={<DeleteIcon />}>
                                    Löschen
                                </Button>
                            </Grid>
                        </Grid>
                }
                        {
                            view !== 'SHOW_ALL' &&
                            <Grid item xs={12} container spacing={2} direction="row" justifyContent="center">
                                <Grid item>
                                    <Button  variant="contained" onClick={() => {
                                        setView('SHOW_ALL')
                                        setErrorMsg(undefined)
                                        fetchCarsData();
                                    }}>
                                        Zurück
                                    </Button>
                                </Grid>
                            </Grid>
                        }

            </Grid>
        </Paper>
    );
}

export default CarManager;