
import { useLayoutEffect, useEffect, useRef, useState, ChangeEvent } from "react";

import axios from "axios";

import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
//import germanyHigh from "@amcharts/amcharts4-geodata/germanyHigh";
import germanyHigh from '../assets/germanyHigh';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import geodata_lang_DE from "@amcharts/amcharts4-geodata/lang/DE"; 

import AddLocationIcon from '@mui/icons-material/AddLocation';
import EditLocationAltRoundedIcon from '@mui/icons-material/EditLocationAltRounded';
import MyLocationRoundedIcon from '@mui/icons-material/MyLocationRounded';
import SearchRoundedIcon from '@mui/icons-material/SearchRounded';
import OpenInFullRoundedIcon from '@mui/icons-material/OpenInFullRounded';

import LocalStorageService, { API_URL, ACCESS_TOKEN_NAME, TOKEN_PREFIX } from '../services/LocalStorageService';

import { InputLabel, Select, MenuItem, Modal, Paper, Grid, TextField, 
    InputAdornment, Button, IconButton } from "@mui/material";

import IStation from "../models/StationModel";
import ITrack from "../models/TrackModel";
import { locations, Pfahrten } from "../models/TaskModel";

import { Stations } from "../assets/staions";

export function TrafficMap() {
    const localStorageService = LocalStorageService.getService();

    const chart = useRef<any>(null);
    const[showStationModal, setShowStationModal] = useState<boolean>(false);
    const[stations, setStations] = useState<Array<IStation>>([])


    let saveStation:IStation = {
        title: "",
        latitude: "",
        longitude: "",
        distance: "",
        previous: "",
        type: "PASSAGE"
    }

   
    const fetchStationData = async () => {
        const result = await axios.get(API_URL + "station/all", {
            headers: {
                ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            }
        });
        setStations(result.data);          
    };

    useEffect(() => {
        fetchStationData();
    }, [])

    const generateTracks = ():Array<Array<IStation>> => {
        let tracks:Array<Array<IStation>> = []
        Stations.forEach((station) => {
            let track:Array<IStation> = []
            if(station.previous) {
                track.push(station);

                const prev = Stations.find(s => s.id === station.previous)

                if(prev) {
                    track.push(prev)
                }
               
                tracks.push(track)
            }

        })
        return tracks;
    }

    useLayoutEffect(() => {

        am4core.useTheme(am4themes_animated);

        let c = am4core.create("trafficchartdiv", am4maps.MapChart);

        c.geodata = germanyHigh;
        c.language.locale = geodata_lang_DE;

        c.projection = new am4maps.projections.Miller();

        let polygonSeries = c.series.push(new am4maps.MapPolygonSeries());

        polygonSeries.useGeodata = true;

        let polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = "{name}";
        polygonTemplate.fill = am4core.color("#74B266");
        polygonTemplate.fill = c.colors.getIndex(0).lighten(0.5);

        let hs = polygonTemplate.states.create("hover");
        hs.properties.fill = c.colors.getIndex(0);

        var imageSeries = c.series.push(new am4maps.MapImageSeries());

        var imageSeriesTemplate = imageSeries.mapImages.template;
        var circle = imageSeriesTemplate.createChild(am4core.Circle);
        circle.radius = 8;
        circle.fill = am4core.color("#e03e96");
        circle.stroke = am4core.color("#FFFFFF");
        circle.strokeWidth = 3;
        circle.nonScaling = true;
        circle.tooltipText = "{title}";

        //circle.events.on('down', () => alert("Circle Clicked"))

        imageSeriesTemplate.propertyFields.latitude = "latitude";
        imageSeriesTemplate.propertyFields.longitude = "longitude";

       imageSeries.data = Stations;

/**
          imageSeries.events.on("beforedatavalidated", function(ev) {
              axios.get(API_URL + "station/all", {
                    headers: {
                        ACCESS_TOKEN_NAME: TOKEN_PREFIX + localStorageService.getAccessToken(),
                        'Access-Control-Allow-Origin': '*',
                        'Content-Type': 'application/json',
                    }
                })
                .then(result => ev.target.data = result.data);
                        
        });
 */
 

        var lineSeries = c.series.push(new am4maps.MapLineSeries());
        lineSeries.mapLines.template.stroke = am4core.color("#e03e96");

        lineSeries.mapLines.template.line.nonScalingStroke = true;
        lineSeries.mapLines.template.line.adapter.add("strokeWidth", function(strokeWidth){
        strokeWidth = 5;
        return strokeWidth;
        });

            lineSeries.data = [{
                "multiGeoLine": generateTracks()
            }];
    

        chart.current = c;

        return () => {
            c.dispose()
        }
    }, [])

    const onCancel = () => {
        setShowStationModal(false);
    }

    const onSave = () => {
        axios.post(API_URL + 'station/save', saveStation)
        .then(resp => {

        })
        .catch(e => {
            alert(e)
        })
    }

    const onChange = ({target: { name, value}}: ChangeEvent<HTMLInputElement>) => {
        if(!name) return;
        if(!value) return;
        //setSaveStation({...saveStation, [name]: value } as IStation);
    };

    const onStationSave = () => {
        axios.post(API_URL + 'station/saveAll', Stations)
        .catch(e => {
            alert(e)
        })
    }

    return(
        <div>
            <div className="Content" id="trafficchartdiv" key="traffic-map"/>        
            <div className="Search-Field">
                <Paper elevation={2} style={{ padding: .2+'em' }}>
                    <TextField
                        id="input-with-icon-textfield"
                        type="text"
                        label={"Suche"}
                        placeholder={"Nach Fahrzeug suchen"}
                        InputProps={{
                        endAdornment: (
                            <InputAdornment position="start">
                                <IconButton>
                                    <SearchRoundedIcon />
                                </IconButton>
                            </InputAdornment>
                        ),
                        }}
                        variant="outlined"
                    />
                </Paper>
            </div>
            <Modal
                keepMounted
                open={showStationModal}
                onClose={() => setShowStationModal(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <div className={"App-header"}>
                <Paper className="Modal">
                    <Grid container spacing={3} direction="column">
                        <Grid item>
                            <h2>Station hinzufügen</h2>
                        </Grid>
                        <Grid item>
                            <TextField
                                 onChange={(e) => {
                                    saveStation.title = e.target.value;
                                }}
                                name="title"
                                id="input-with-icon-textfield"
                                label={"Bezeichnung"}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <EditLocationAltRoundedIcon />
                                    </InputAdornment>
                                ),
                                }}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item>
                            <TextField
                                onChange={(e) => {
                                    saveStation.longitude = e.target.value;
                                }}
                                name="longitude"
                                id="input-with-icon-textfield"
                                label={"Longitude"}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <MyLocationRoundedIcon />
                                    </InputAdornment>
                                ),
                                }}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item>
                            <TextField
                                onChange={(e) => {
                                    saveStation.latitude = e.target.value;
                                }}
                                name="latitude"
                                id="input-with-icon-textfield"
                                label={"Latitude"}
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <MyLocationRoundedIcon />
                                    </InputAdornment>
                                ),
                                }}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item>
                            <TextField
                                onChange={(e) => {
                                    saveStation.distance = e.target.value;
                                }}
                                name="distance"
                                id="input-with-icon-textfield"
                                label={"Distanz"}
                                
                                type="number"
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <OpenInFullRoundedIcon />
                                    </InputAdornment>
                                ),
                                }}
                                variant="outlined"
                            />
                        </Grid>
                        <Grid item>
                        <InputLabel id="demo-simple-select-label">Typ</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                name="type"
                                onChange={(e) => {
                                    saveStation.type = e.target.value === "DEPARTURE" ? "DEPARTURE" : "PASSAGE"
                                }}
                                style={{ width: 15+'em' }}
                            >
                                <MenuItem value={"DEPARTURE"}>Abfahrt</MenuItem>
                                <MenuItem value={"PASSAGE"}>Durchfahrt</MenuItem>
                            </Select>
                        </Grid>
                        { stations && stations.length > 0 &&
                        <Grid item>
                        <InputLabel id="demo-simple-select-label">Vorherige Station</InputLabel>
                            <Select
                                onChange={(e) => {
                                   e.target.value && (saveStation.previous = e.target.value as string);
                                }}
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                style={{ width: 15+'em' }}
                            >
                                {
                                    stations && stations.map((station) => 
                                        <MenuItem value={station.id}>{station.title}</MenuItem>
                                    )
                                }
                            </Select>
                        </Grid>
                        }
                        <Grid item>
                            <Button onClick={onCancel} variant="contained" color="error" style={{ marginRight: 4+'em' }}>
                                Abbrechen
                            </Button>
                            <Button onClick={onSave} variant="contained" color="success" style={{ marginLeft: 4+'em' }}>
                                Speichern
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
                </div>
            </Modal>

            <div className="ActionButton">
                <Button onClick={onStationSave} variant="contained" color="primary">
                    Save
                </Button>
                <IconButton onClick={() => setShowStationModal(true)} size="large" color="info">
                    <AddLocationIcon fontSize="large" color="warning" style={{ fontSize: 4+'rem' }}/>
                </IconButton>
                Station hinzufügen
            </div>
        </div>
    );
}

export default TrafficMap;
