import { useState, useEffect } from "react";

import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';

import AssignmentRoundedIcon from '@mui/icons-material/AssignmentRounded';
import MapRoundedIcon from '@mui/icons-material/MapRounded';
import SupervisorAccountRoundedIcon from '@mui/icons-material/SupervisorAccountRounded';
import SettingsRoundedIcon from '@mui/icons-material/SettingsRounded';
import QueryStatsIcon from '@mui/icons-material/QueryStats';
import DirectionsCarRoundedIcon from '@mui/icons-material/DirectionsCarRounded';

import Statistics from "./Statistics";
import Bill from './Bill';
import TrafficMap from "./TrafficMap";
import UserManager from "./UserManager";
import Profile from "./Profile";
import CarManager from "./CarManager";
import Simulator from "./Simulator";

import LocalStorageService from "../services/LocalStorageService";

export function SideMenu({updateView}:{updateView:Function}) {

    const[viewComponent, setViewComponent] = useState<React.ReactElement>();

    const localStorageService = LocalStorageService.getService();
    const roles = localStorageService.getUserRoles();

    useEffect(() => {
        updateView(viewComponent);
    },[updateView, viewComponent]);

    return (
        <div className="SideMenu">
            <Grid container spacing={3} direction="column" justifyContent="center">
                <Grid item xs={12}>
                        <MenuEntry title={"Rechnungen"} icon={<AssignmentRoundedIcon />} onViewSwitch={() => setViewComponent(<Bill />)}/>
                </Grid>                
                <Grid item xs={12}>
                        <MenuEntry title={"Karte"} icon={<MapRoundedIcon />} onViewSwitch={() => setViewComponent(<TrafficMap />)}/>
                </Grid>
                {
                    roles && roles.indexOf('ADMIN') > -1 &&
                    <>
                    <Grid item xs={12}>
                        <MenuEntry title={"Statistik"} icon={<QueryStatsIcon />} onViewSwitch={() => setViewComponent(<Statistics />)}/>
                    </Grid>
                    <Grid item xs={12}>
                            <MenuEntry title={"Nutzerverwaltung"} icon={<SupervisorAccountRoundedIcon />} onViewSwitch={() => setViewComponent(<UserManager />)}/>
                    </Grid>
                    <Grid item xs={12}>
                            <MenuEntry title={"Fahrzeugverwaltung"} icon={<DirectionsCarRoundedIcon />} onViewSwitch={() => setViewComponent(<CarManager />)}/>
                    </Grid>
                    <Grid item xs={12}>
                            <MenuEntry color="success" title={"Simulator"} icon={<DirectionsCarRoundedIcon />} onViewSwitch={() => setViewComponent(<Simulator />)}/>
                    </Grid>
                    </>
                }
            </Grid>
            <div className="Settings">
                <MenuEntry title={"Profil"} icon={<SettingsRoundedIcon />} onViewSwitch={() => setViewComponent(<Profile />)}/>
            </div>
        </div>
    );
}

export function MenuEntry({title,onViewSwitch,icon, color}:{title:string,onViewSwitch:Function,icon:React.ReactElement, 
    color?:"inherit" | "error" | "success" | "info" | "primary" | "secondary" | "warning"}) {

    if(!color) {
        color="error"
    }

    return (

            <Button color={color} startIcon={icon} variant="contained" onClick={() => onViewSwitch()} style={{ backgroundColor: "#b83209",width: 20+'em' }}>
                <h3>{title}</h3>
            </Button>
    
    );
}

export default SideMenu;