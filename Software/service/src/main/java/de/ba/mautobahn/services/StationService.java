package de.ba.mautobahn.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ba.mautobahn.models.Station;
import de.ba.mautobahn.models.dto.StationDTO;
import de.ba.mautobahn.repositories.StationRepository;

@Service
public class StationService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private StationRepository stationRepository;

	@Autowired
	public StationService(StationRepository stationRepository) {
		super();
		this.stationRepository = stationRepository;
	}

	public boolean save(StationDTO dto) {

		Station station = dtoToStation(dto);
		
		Optional<Station> prev = stationRepository.findById(dto.getPrevious());
		
		if(prev.isPresent()) {
			station.setPrevious(prev.get());
		}
	
		logger.debug("Saved Station: {}",station);
		
		stationRepository.save(station);
		
		return true;
	}

	public boolean saveAll(List<StationDTO> stations) {
		
		for(StationDTO dto : stations) {
			stationRepository.save(dtoToStation(dto));
		}
		
		for(StationDTO s : stations) {
			
			Optional<Station> prev = stationRepository.findById(s.getPrevious());
			
			if(prev.isPresent()) {
				Optional<Station> stat = stationRepository.findById((long) s.getId());
				
				if(stat.isPresent()) {
					Station station = stat.get();
					station.setPrevious(prev.get());
					stationRepository.save(station);
				}
			}
			
		}
		
		return false;
	}

	private Station dtoToStation(StationDTO dto) {
		Station station = new Station();
		
		station.setId((long) dto.getId());
		station.setTitle(dto.getTitle());
		station.setType(dto.getType());
		station.setDistance(dto.getDistance());
		station.setLongitude(dto.getLongitude());
		station.setLatitude(dto.getLatitude());
		
		return station;
	}

}
