package de.ba.mautobahn.repositories;

import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.Station;

public interface StationRepository extends CrudRepository<Station, Long> {
	
}
