package de.ba.mautobahn.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.Action;
import de.ba.mautobahn.repositories.ActionRepository;
import de.ba.mautobahn.services.VehicleService;

@RestController
@RequestMapping("/action")
@CrossOrigin(origins = "http://localhost:3000")
public class ActionController {
	
	private ActionRepository actionRepository;
	private VehicleService vehicleService;

	@Autowired
	public ActionController(ActionRepository actionRepository, VehicleService vehicleService) {
		super();
		this.actionRepository = actionRepository;
		this.vehicleService = vehicleService;
	}

	@PostMapping("/update")
	public String updateAction(@RequestBody @Validated Action action) {
		return vehicleService.updateVehicle(action);
	}
	
	@GetMapping("/all")
	public List<Action> getAllActions() {
		List<Action> all = new ArrayList<>();
		
		for(Action a : actionRepository.findAll()) {
			all.add(a);
		}
		
		return all;
	}
}
