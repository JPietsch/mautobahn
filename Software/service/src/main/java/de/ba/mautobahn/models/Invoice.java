package de.ba.mautobahn.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "invoice")
public class Invoice {
	
	@TableGenerator(name = "InvID", table = "ID_GEN", pkColumnName = "GEN_NAME", valueColumnName = "GEN_VAL", pkColumnValue = "Addr_Gen", initialValue = 10000, allocationSize = 100)
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "InvID")
	private Long invoiceID;

	
//	@Column
//	@JsonIgnore
//	private Long userID;
	
	
	@Column
	private LocalDate RDatum;
	
	
	@ManyToOne
	@JoinColumn(name="userID", nullable=false)
	private User user;

	@Transient
	private String steuersatz;

	@Transient
	private String bruttoSumme;

	@Transient
	private String nettoSumme;

	// @Transient
	// private HashMap<String, List<Track>> tracktable = new HashMap<String,
	// List<Track>>();
	
	@JacksonXmlElementWrapper(localName = "InvoicePositions")
	@Transient
	private List<InvoicePosition> InvoicePosition = new ArrayList<>();

	public Invoice(User user, double steuersatz, double bruttoSumme, double nettoSumme,
			List<InvoicePosition> invPosList) {
		super();
		DecimalFormat formatter = new DecimalFormat("#.##");
		DecimalFormat df2 = new DecimalFormat(" #,##0.00 '%'");
		this.user = user;
		this.steuersatz = df2.format((steuersatz - 1.0) * 100.0);
		this.bruttoSumme = formatter.format(bruttoSumme);
		this.nettoSumme = formatter.format(nettoSumme);
		this.InvoicePosition = invPosList;
	}

	public Invoice() {
	}

	public Long getInvoiceID() {
		return invoiceID;
	}

	public void setInvoiceID(Long invoiceID) {
		this.invoiceID = invoiceID;
	}

	@JsonIgnore
	public List<InvoicePosition> getInvoicePosition() {
		return InvoicePosition;
	}

	public void setInvoicePosition(List<InvoicePosition> invoicePosition) {
		InvoicePosition = invoicePosition;
	}

	public void setSteuersatz(String steuersatz) {
		this.steuersatz = steuersatz;
	}

	public void setBruttoSumme(String bruttoSumme) {
		this.bruttoSumme = bruttoSumme;
	}

	public void setNettoSumme(String nettoSumme) {
		this.nettoSumme = nettoSumme;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSteuersatz() {
		return steuersatz;
	}

	public void setSteuersatz(double steuersatz) {
		DecimalFormat df2 = new DecimalFormat(" #,##0.00 '%'");
		this.steuersatz = df2.format((steuersatz - 1.0) * 100.0);
	}

	public String getBruttoSumme() {
		return bruttoSumme;
	}

	public void setBruttoSumme(double bruttoSumme) {
		DecimalFormat formatter = new DecimalFormat("#.##");
		this.bruttoSumme = formatter.format(bruttoSumme);
	}

	public String getNettoSumme() {
		return nettoSumme;
	}

	public void setNettoSumme(double nettoSumme) {
		DecimalFormat formatter = new DecimalFormat("#.##");
		this.nettoSumme = formatter.format(nettoSumme);
	}

//	public Long getuserID() {
//		return userID;
//	}
//
//	public void setuserID(Long userID) {
//		this.userID = userID;
//	}

	

	public LocalDate getRDatum() {
		return RDatum;
	}

	public void setRDatum(LocalDate rDatum) {
		RDatum = rDatum;
	}

	@Override
	public String toString() {
		return "Invoice [invoiceID=" + invoiceID + ", user=" + user.getFirstName() + " " + user.getLastName()
				+ ", steuersatz=" + steuersatz + ", bruttoSumme=" + bruttoSumme + ", nettoSumme=" + nettoSumme
				+ ", InvoicePosition=" + InvoicePosition.get(0).getTrack() + "]";
	}

}