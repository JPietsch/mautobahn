package de.ba.mautobahn.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.User;
import de.ba.mautobahn.repositories.UserRepository;
import de.ba.mautobahn.services.UserService;


@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {
	
	private UserRepository userRepository;
	private UserService userService;
	
	@Autowired
	public UserController(UserRepository userRepository, UserService userService) {
		super();
		this.userRepository = userRepository;
		this.userService = userService;
	}
	
	@GetMapping("/{id}")
	public User getUserById(@PathVariable Long id) {
		Optional<User> entry = userRepository.findById(id);
		System.out.println("Found user "+entry);
		return entry.get();
	}

	@GetMapping("/all")
	public List<User> getAllUser() {
		List<User> all = new ArrayList<>();
		
		for(User u : userRepository.findAll()) {
			all.add(u);
		}
		
		return all;
	}
	
	@PostMapping("/save")
	public boolean saveUser(@RequestBody @Validated User user) {
		System.out.println(user);
		try {
			userService.save(user);
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteUserById(@PathVariable Long id) {

		try {			
			userRepository.deleteById(id);
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
}



