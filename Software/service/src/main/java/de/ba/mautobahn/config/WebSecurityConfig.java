package de.ba.mautobahn.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import de.ba.mautobahn.security.JwtAuthenticationEntryPoint;
import de.ba.mautobahn.security.JwtAuthenticationProvider;
import de.ba.mautobahn.security.JwtAuthenticationTokenFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;
	
	@Autowired
	private JwtAuthenticationProvider jwtAuthenticationProvider;
	
	@Autowired
	public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) {
		authenticationManagerBuilder.authenticationProvider(jwtAuthenticationProvider);
	}

	@Bean
	public JwtAuthenticationTokenFilter auhAuthenticationTokenFilter() {
		return new JwtAuthenticationTokenFilter();
	}
	
	@Bean
	public CorsConfigurationSource configurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedHeaders(List.of("Authorization", "Cache-Control", "Content-Type"));
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
        configuration.setAllowedMethods(Arrays.asList("HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE"));
        configuration.setAllowCredentials(true);
       
        configuration.setExposedHeaders(List.of("Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
	}
	
//	  @Bean
//	  CorsConfigurationSource corsConfigurationSource() {
//	      UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//	      CorsConfiguration config = new CorsConfiguration();
//	      config.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
//	      config.setAllowedMethods(Arrays.asList("*"));
//	      config.setAllowedHeaders(Arrays.asList("*"));
//	      config.setAllowCredentials(true);
//	      config.applyPermitDefaultValues();
//	      
//	      source.registerCorsConfiguration("/**", config);
//	      return source;
//	}


	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.csrf().disable()
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.authorizeRequests()
			.antMatchers("/login").permitAll()
			.antMatchers("/user/**").permitAll()
			.antMatchers("/statistics").permitAll()
			.antMatchers("/station/**").permitAll()
			.antMatchers("/track/**").permitAll()
			.antMatchers("/action/**").permitAll()
			.antMatchers("/vehicle/**").permitAll()
			.antMatchers("/invoice/**").permitAll()
			.antMatchers("/h2-console/**").permitAll()
			.anyRequest().authenticated();
		
		httpSecurity.addFilterBefore(auhAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
		httpSecurity.headers().cacheControl();
	}
	
}
