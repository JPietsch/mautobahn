package de.ba.mautobahn.repositories;

import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.Action;

public interface ActionRepository extends CrudRepository<Action, Long>{

}
