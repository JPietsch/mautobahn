package de.ba.mautobahn.models.dto;

import de.ba.mautobahn.models.StationType;

public class StationDTO {

	private int id;
	private String title;
	private double latitude;
	private double longitude;
	private double distance;
	private long previous;
	private StationType type;
	
	public StationDTO() {}

	public StationDTO(int id, String title, double latitude, double longitude, double distance, long previous,
			StationType type) {
		super();
		this.id = id;
		this.title = title;
		this.latitude = latitude;
		this.longitude = longitude;
		this.distance = distance;
		this.previous = previous;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public long getPrevious() {
		return previous;
	}

	public void setPrevious(long previous) {
		this.previous = previous;
	}

	public StationType getType() {
		return type;
	}

	public void setType(StationType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "StationDTO [id=" + id + ", title=" + title + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", distance=" + distance + ", previous=" + previous + ", type=" + type + "]";
	}
		
}
