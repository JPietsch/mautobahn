package de.ba.mautobahn.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;



public class InvoicePosition {

    @JacksonXmlElementWrapper(localName = "tracks", useWrapping = true)
    private List<Track> track = new ArrayList<>();

    private String licensePlateCode;

    private int pos;


    @Override
    public String toString() {
        return "InvoicePosition [licensePlateCode=" + licensePlateCode + "]";
    }




    public InvoicePosition(List<Track> track, String licensePlateCode, int pos) {
        super();
        this.track = track;
        this.licensePlateCode = licensePlateCode;
        this.pos = pos;
    }




    public List<Track> getTrack() {
        return track;
    }




    public void setTrack(List<Track> track) {
        this.track = track;
    }




    public String getLicensePlateCode() {
        return licensePlateCode;
    }




    public void setLicensePlateCode(String licensePlateCode) {
        this.licensePlateCode = licensePlateCode;
    }

    public void addTrack(Track t) {
        if (t == null) return;
        this.track.add(t);
    }




    public int getPos() {
        return pos;
    }




    public void setPos(int pos) {
        this.pos = pos;
    }




}