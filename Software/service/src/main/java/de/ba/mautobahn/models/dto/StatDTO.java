package de.ba.mautobahn.models.dto;

public class StatDTO {
	
	private String country;
	private int value;
	
	public StatDTO() {}
	
	public StatDTO(String country, int value) {
		super();
		this.country = country;
		this.value = value;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "StatDTO [country=" + country + ", value=" + value + "]";
	}
	
}
