package de.ba.mautobahn.services;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ba.mautobahn.models.Track;
import de.ba.mautobahn.repositories.TrackRepository;
import de.ba.mautobahn.repositories.VehicleRepository;

@Service
public class StatisticsService {
	
	private UserService userService;
	private VehicleRepository vehicleRepository;
	private Track track;
	private TrackRepository trackRepository;
	
	@Autowired
	public StatisticsService(UserService userService, VehicleRepository vehicleRepository,
			TrackRepository trackRepository) {
		super();
		this.userService = userService;
		this.vehicleRepository = vehicleRepository;
		this.trackRepository = trackRepository;
	}


	public boolean generate(LocalDateTime onDate, LocalDateTime offDate) {
		
		List<Track> licensePlateCodes = trackRepository.findInTimeRange(onDate, offDate);
	//	Iterable<Vehicle> test = trackRepository.findBy;
		//System.out.println(test);
		return true;
	}

}
