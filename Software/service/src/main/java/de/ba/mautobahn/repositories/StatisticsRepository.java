package de.ba.mautobahn.repositories;

import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.Statistics;

public interface StatisticsRepository extends CrudRepository<Statistics,Long> {
	
}
