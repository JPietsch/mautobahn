package de.ba.mautobahn.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.User;
import de.ba.mautobahn.models.Vehicle;

public interface UserRepository extends CrudRepository<User, Long> {	
	
	@Query(value = "SELECT * "
			+ "FROM VEHICLE "
			+ "WHERE OWNER_ID = ?1", nativeQuery = true)
	List<Vehicle> findVehicleByOwnerID(long id);
	
	//Select * From USERS Left Join VEHICLE On Users.id = Vehicle.owner Left Join Track on Vehicle.License_Plate_Code = Track.Vehicle Where Track.id = 1
	@Query(value = "SELECT * FROM Users left join Vehicle on users.id=vehicle.owner left join Track on Vehicle.License_Plate_Code = Track.Vehicle WHERE Track.OFF_DATE BETWEEN 20161023 AND 20161023" , nativeQuery = true)
	User getInvoiceUser();
	
	//
	
	//@Query(value = "SELECT DISTINCT a FROM Vehicle v, User a left join fetch a.vehicles left join v.tracks where a.id=815 AND Year(OFF_Date) = 2019 AND Month(OFF_Date) = 10")
	//List<User> getInvoiceUser1();

	
	Optional<User> findById(Long id);


	Optional<User> findByUsername(String username);
	
}
