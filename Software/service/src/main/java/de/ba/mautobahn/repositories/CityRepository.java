package de.ba.mautobahn.repositories;

import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.City;

public interface CityRepository extends CrudRepository<City, Long> {

}
