package de.ba.mautobahn.controller;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.Statistics;
import de.ba.mautobahn.models.dto.StatDTO;
import de.ba.mautobahn.repositories.StatisticsRepository;
import de.ba.mautobahn.services.StatisticsService;

@RestController
@RequestMapping("/statistics")
@CrossOrigin(origins = "http://localhost:3000")
public class StatisticsController {

	private StatisticsService statisticsService;
	private StatisticsRepository statisticsRepository;

	@Autowired
	public StatisticsController(StatisticsService statisticsService, StatisticsRepository statisticsRepository) {
		super();
		this.statisticsService = statisticsService;
		this.statisticsRepository = statisticsRepository;
	}
	
	@GetMapping
	public List<StatDTO> getStats() {
		List<StatDTO> stats = new ArrayList<>();
		
		stats.add(new StatDTO("Deutschland", 543));
		stats.add(new StatDTO("England", 232));
		stats.add(new StatDTO("Sonstige", 54));
		
		return stats;
	}
	
	@GetMapping("/test")
	public boolean getStatistic(@RequestParam(value = "onDate") String onDateString, @RequestParam(value = "offDate") String offDateString) throws ParseException {
		LocalDateTime onDate = LocalDateTime.parse(onDateString);
		LocalDateTime offDate = LocalDateTime.parse(offDateString);
		return statisticsService.generate(onDate, offDate);
	}

	@PostMapping("/save")
	public String saveStatistics(@RequestBody Statistics statistics) {
		statisticsRepository.save(statistics);
		return "successfull";
	}
	

}
