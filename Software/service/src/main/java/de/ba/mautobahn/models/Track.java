package de.ba.mautobahn.models;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "track")
public class Track {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long tid;
    
    @OneToOne
    private Station start; 
    
    @OneToOne
    private Station end;
    
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="vehicle", nullable=false)
    private Vehicle vehicle;
    
	@Column
    private float distance;
    
    @Column
    private LocalDateTime onDate;
    
    @Column
    private LocalDateTime offDate;
    
    
    @Transient
    private double price; 
    
    public Track() {}
	
	public Track(Long id, Station start, Station end, float distance, LocalDateTime onDate, LocalDateTime offDate) {
		super();
		this.tid = id;
		this.start = start;
		this.end = end;
		this.distance = distance;
		this.onDate = onDate;
		this.offDate = offDate;
	}

	public Track(Long id, Station start, Station end, float distance, LocalDateTime onDate, LocalDateTime offDate, Vehicle vehicle) {
		super();
		this.tid = id;
		this.start = start;
		this.end = end;
		this.distance = distance;
		this.onDate = onDate;
		this.offDate = offDate;
		this.vehicle = vehicle;
	}

	public Track(Station start, LocalDateTime date, Vehicle vehicle) {
		super();
		this.start = start;
		this.end = start;
		this.onDate = date;
		this.offDate = date;
		this.vehicle = vehicle;
	}



	public Long getTid() {
		return tid;
	}

	public void setTid(Long tid) {
		this.tid = tid;
	}

	public Station getStart() {
		return start;
	}

	public void setStart(Station start) {
		this.start = start;
	}

	public Station getEnd() {
		return end;
	}

	public void setEnd(Station end) {
		this.end = end;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public LocalDateTime getOnDate() {
		return onDate;
	}

	public void setOnDate(LocalDateTime onDate) {
		this.onDate = onDate;
	}

	public LocalDateTime getOffDate() {
		return offDate;
	}

	public void setOffDate(LocalDateTime offDate) {
		this.offDate = offDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Track [id=" + tid + ", start=" + start + ", end=" + end + ", distance=" + distance + ", onDate=" + onDate
				+ ", offDate=" + offDate +", vehicle=" + vehicle.getLicensePlateCode() + "]";
	}

}
