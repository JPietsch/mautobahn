package de.ba.mautobahn.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.Track;
import de.ba.mautobahn.repositories.TrackRepository;

@RestController
@RequestMapping("/track")
@CrossOrigin(origins = "http://localhost:3000")
public class TrackController {
	
	private TrackRepository trackRepository;
	
	@Autowired
	public TrackController(TrackRepository trackRepository) {
		this.trackRepository = trackRepository;
	}

	@GetMapping("/test")
	public Track testTrack() {
		return new Track(null, LocalDateTime.now(),null);
	}
	
	@GetMapping("/{id}")
	public Track getTrackById(@PathVariable Long id) {
		Optional<Track> entry = trackRepository.findById(id);
		
		return entry.get();
	}
	
	@GetMapping("/all")
	public List<Track> getAllTrack() {
		List<Track> all = new ArrayList<>();

		for(Track t : trackRepository.findAll()) {
			all.add(t);
			System.out.println(t);
		}
		
		return all;
	}
	
	@PostMapping("/save")
	public boolean saveTrack(@RequestBody @Validated Track track) {
		trackRepository.save(track);
		return true;
	}
	
	@DeleteMapping("/delete")
	public boolean deleteTrackById(Long id) {
		trackRepository.deleteById(id);
		return true;
	}
	
}
