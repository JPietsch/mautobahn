package de.ba.mautobahn.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.Station;
import de.ba.mautobahn.models.dto.StationDTO;
import de.ba.mautobahn.repositories.StationRepository;
import de.ba.mautobahn.services.StationService;

@RestController
@RequestMapping("/station")
@CrossOrigin(origins = "http://localhost:3000")
public class StationController {

	private StationRepository stationRepository;
	private StationService stationService;
	
	@Autowired
	public StationController(StationRepository stationRepository, StationService stationService) {
		super();
		this.stationRepository = stationRepository;
		this.stationService = stationService;
	}
	
	@GetMapping("/{id}")
	public Station getStationById(@PathVariable Long id) {
		return stationRepository.findById(id).get();
	}
	


	@GetMapping("/all")
	public List<Station> getAllStations() {
		List<Station> all = new ArrayList<>();
		
		for(Station s : stationRepository.findAll()) {
			all.add(s);
		}
		
		return all;
	}
	
	@PostMapping("/save")
	public boolean saveStation(@RequestBody @Validated StationDTO station) {
		return stationService.save(station);
	}
	
	@PostMapping("/saveAll")
	public boolean saveStation(@RequestBody @Validated List<StationDTO> station) {
		return stationService.saveAll(station);
	}
	
	
	@DeleteMapping("/{id}")
	public boolean deleteStation(@PathVariable Long id) {
		stationRepository.deleteById(id);
		return true;
	}
}



