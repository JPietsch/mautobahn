package de.ba.mautobahn.models.dto;

public class JWTTokenResponse {
    private String accessToken;

    public JWTTokenResponse(String token) {
        this.accessToken = token;
    }

    public String getToken() {
        return accessToken;
    }

    public void setToken(String token) {
        this.accessToken = token;
    }
}
