package de.ba.mautobahn.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ba.mautobahn.repositories.TrackRepository;

@Service
public class TrackService {

	private TrackRepository trackRepository;
	
	@Autowired
	public TrackService(TrackRepository trackRepository) {
		this.trackRepository = trackRepository;
	}
	
	/**
	 * @needs data set from highway tracker
	 * @updates current information for the given KFZ
	 * @param new_checkpoint
	 */
	public void updateTrack(int new_checkpoint) {
	/**	
		//check for last status, maybe sql select for true/false if not directly readable
		boolean isOnRoad = Vehicle.isOnRoad();
		
		//if true add the driven distance
		//if false create new track
		if (isOnRead) {
			addDistanz(new_checkpoint);
			if(departure) {
				closeTrackCase();
			}
		} else {
			createTrack(new_checkpoint);
		}
	 */
		
	}

	/**
	 * @needs data set from highway tracker
	 * @create current information for the given KFZ
	 * @param new_checkpoint
	 */
	public void createTrack(int new_checkpoint) {
		/**
		//get KFZ number and time of driving through
		String KFZ = getKFZ();
		int timestamp = getTimestamp();

		//create new data set for the ride
		@Query(value="Insert into Track (KFZ, lastTimeTracked, Start, End, Distance, isOnRoad) values (?1, ?2, ?3, ?3, 0, true)", nativeQuery=true)
		Track getDistanceOfLastTrack(KFZ, timestamp, new_checkpoint);
		*/
	}

	/**
	 * @needs data set from highway tracker
	 * @adds driven distance onto the already given information for the KFZ
	 * @param new_checkpoint
	 */
	public void addDistanz(int new_checkpoint) {
		/**
		 
		//get the distance of new track part
		int length = getlength(new_checkpoint);
				
		//get the last driven distance
		@Query(value="select distance from Track where KFZ = ?1 Sort by timestamp DESC Limit 1", nativeQuery=true)
		Track getDistanceOfLastTrack(KFZ);

		//add new track part to distance
		distance += length;
		
		//update data set for the ride
		@Query(value="update Track set lastTimeTracked = ?2, End = ?3, Distance = ?4 where KFZ == ?1 sort by lastTimeTracked DESC Limit 1", nativeQuery=true)
		Track setDistanceAndTimeOfLastTrack(KFZ, timestamp, new_checkpoint, distance);
		
		 */
	}

	/**
	 * @needs two checkpoints
	 * @get distance between given checkpoints
	 * @param new_checkpoint
	 */
	private int getlength(int new_checkpoint) {
		/**
		 
		//get current location
		int current_location = Vehicle.getLocation();
		
		//request length between two checkpoints
		@Query(value="select length from Track where start = ?1 and end ?2", nativeQuery=true)
		Track getLengthByCheckpoints(current_location, new_checkpoint);
		int length = ;
		
		return length;
		 */
		return 0;
		
	}

	/**
	 * @needs data set from highway tracker
	 * @updates current information for the given KFZ
	 * @param new_checkpoint
	 */
	public void closeTrackCase() {
		/**
		//end data set for the ride
		@Query(value="update Track set isOnRoad = false where KFZ == ?1 and lastTimeTracked = ?2", nativeQuery=true)
		Track setEndOfLastTrack(KFZ, timestamp);
		*/
	}
	
}
