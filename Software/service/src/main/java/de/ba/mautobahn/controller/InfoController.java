package de.ba.mautobahn.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.InfoResponse;

@RestController
@RequestMapping("/info")
public class InfoController {
	
	@GetMapping
	public InfoResponse getInfo() {
		return new InfoResponse("Demo info");
	}

}
