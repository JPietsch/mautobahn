package de.ba.mautobahn.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import de.ba.mautobahn.services.JwtTokenService;
import io.jsonwebtoken.JwtException;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
	
	private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationProvider.class);
	
	private final JwtTokenService jwtService;
	
	@Autowired
	public JwtAuthenticationProvider(JwtTokenService jwtService) {
		super();
		this.jwtService = jwtService;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		try {
			
			String token = (String) authentication.getCredentials();
			String username = jwtService.getUsernameFromToken(token);
			
			return jwtService.validateToken(token)
					.map(aBoolean -> new JwtAuthenticatedProfile(username))
					.orElseThrow(() -> new JwtAuthenticationException("JWT Token validation failed"));
			
		} catch (JwtException e) {
			log.error(String.format("Invalid JWT Token: %s", e.getMessage()));
			throw new JwtAuthenticationException("Failed to verify token!");
		}
		

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return JwtAuthentication.class.equals(authentication);
	}

}
