package de.ba.mautobahn.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="vehicle")
public class Vehicle {
	
	
//    @GeneratedValue(strategy=GenerationType.AUTO)
//    @Column(nullable = false, updatable = false)
//	private long id;
	
	@Id
	@Column(nullable = false, updatable = false)
    private String licensePlateCode;
    
    @Column
    private boolean isOnRoad;
        
    //@OneToOne
    @ManyToOne
    @JoinColumn(name="owner", nullable=false)
    @JsonIgnore
    private User owner;
    
    
  //@Transient
    @OneToMany(mappedBy = "vehicle", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonBackReference
    private List<Track> tracks = new ArrayList<>();
    
    public Vehicle() {}
    
	public Vehicle(String licensePlateCode, boolean isOnRoad, User owner, List<Track> tracks) {
		super();
		this.licensePlateCode = licensePlateCode;
		this.isOnRoad = isOnRoad;
		this.owner = owner;
		this.tracks = tracks;
	}

	public Vehicle(String licensePlateCode, boolean isOnRoad, User owner) {
		super();
		this.licensePlateCode = licensePlateCode;
		this.isOnRoad = isOnRoad;
		this.owner = owner;
	}

	public String getLicensePlateCode() {
		return licensePlateCode;
	}

	public void setLicensePlateCode(String licensePlateCode) {
		this.licensePlateCode = licensePlateCode;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public boolean isOnRoad() {
		return isOnRoad;
	}

	public void setOnRoad(boolean isOnRoad) {
		this.isOnRoad = isOnRoad;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public void addTrack(Track track) {
		if(track == null) return;
		if(this.tracks.contains(track)) return;
		this.tracks.add(track);
	}
	
	public void removeTrack(Track track) {
		this.tracks.remove(track);
	}
	
	public Track getLastTrack() {
		Track track = new Track(null, LocalDateTime.MIN, null);
		for(Track t : this.tracks) {
			if(t.getOffDate().isAfter(track.getOffDate())) {
				track = t;
			}
		}
		if(track.getStart() != null) {
			return track;
		} else {
			return null;
		}

	}

	@Override
	public String toString() {
		return "Vehicle [licensePlateCode=" + licensePlateCode + ", isOnRoad=" + isOnRoad + ", owner=" + owner
				+ ", tracks=" + tracks + "]";
	}

	
}
