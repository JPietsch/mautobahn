package de.ba.mautobahn.services;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.ba.mautobahn.models.Action;
import de.ba.mautobahn.models.Station;
import de.ba.mautobahn.models.StationType;
import de.ba.mautobahn.models.Track;
import de.ba.mautobahn.models.User;
import de.ba.mautobahn.models.Vehicle;
import de.ba.mautobahn.models.dto.VehicleDTO;
import de.ba.mautobahn.repositories.StationRepository;
import de.ba.mautobahn.repositories.TrackRepository;
import de.ba.mautobahn.repositories.UserRepository;
import de.ba.mautobahn.repositories.VehicleRepository;

@Service
public class VehicleService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private VehicleRepository vehicleRepository;
	private StationRepository stationRepository;
	private TrackRepository trackRepository;
	private UserRepository userRepository;

	@Autowired
	public VehicleService(VehicleRepository vehicleRepository, StationRepository stationRepository,
			TrackRepository trackRepository, UserRepository userRepository) {
		super();
		this.vehicleRepository = vehicleRepository;
		this.stationRepository = stationRepository;
		this.trackRepository = trackRepository;
		this.userRepository = userRepository;
	}
	
	public boolean save(VehicleDTO dto) {
		if(dto == null) return false;
		
		Vehicle v = new Vehicle();
		
		v.setLicensePlateCode(dto.getLicensePlateCode());
		v.setOnRoad(dto.isOnRoad());
		
		
		Optional<User> owner = userRepository.findById(dto.getOwner());
		
		if(owner.isPresent()) {
			v.setOwner(owner.get());
		} else if(dto.getUser() != null) {
			userRepository.save(dto.getUser());
			Optional<User> user = userRepository.findByUsername(dto.getUser().getUsername());
			
			if(user.isPresent()) {
				v.setOwner(user.get());
			}
			
		}
		
		if(v.getOwner() != null) {
			logger.info("Saving car: {}",v);
			vehicleRepository.save(v);
		} else {
			return false;
		}
		return true;
	}
	
	public String updateVehicle(Action action) {
		
		logger.info("Incomming Action: {}",action);
        
        Optional<Station> s = stationRepository.findById(action.getStationId());
        Vehicle vehicle = vehicleRepository.findByLicensePlateCode(action.getLicensePlateCode());
      
        if(!s.isPresent()) return "keine Station";
        if(vehicle == null) return "kein Pkw";
                
        Station station = s.get();
        Track lastTrack = vehicle.getLastTrack();

        if(station.getType() == StationType.PASSAGE) {
            if(!vehicle.isOnRoad()) {
                Track track = new Track(station, action.getTimestamp(), vehicle);
                
                vehicle.addTrack(track);
                lastTrack = track;
                
                vehicle.setOnRoad(true);
            } else {
                
                if (lastTrack == null) {
                    return "kein letzter Track";
                }
                
                if (station.getPrevious() == null) {
                    vehicle.setOnRoad(false);
                    vehicleRepository.save(vehicle);
                    
                    try {
                        trackRepository.deleteById(lastTrack.getTid());
					} catch (Exception e) {
						logger.error("Cannot delete Track {}",lastTrack);
					}
                    
                    vehicle.getTracks().remove(lastTrack);
                    
                    return "Auffahrt hat keinen Vorgänger 1";
                }
                
                if (lastTrack.getEnd().getId() == station.getPrevious().getId()) {
                    lastTrack.setEnd(station);
                    lastTrack.setOffDate(action.getTimestamp());
                    lastTrack.setDistance((float) (lastTrack.getDistance() + station.getDistance()));
                } else {
                    vehicle.setOnRoad(false);
                    vehicleRepository.save(vehicle);
                    trackRepository.deleteById(lastTrack.getTid());
                    vehicle.getTracks().remove(lastTrack);
                    return " falscher Vorgänger 1";
                }
                
            }
        }
        
        if(station.getType() == StationType.DEPARTURE) {
            vehicle.setOnRoad(false);
            
            if (lastTrack == null) {
                return "kein letzter Track";
            }
            
            if (station.getPrevious() == null) {

                vehicleRepository.save(vehicle);
                trackRepository.deleteById(lastTrack.getTid());
                vehicle.getTracks().remove(lastTrack);
                return "Auffahrt hat keinen Vorgänger 2";
            }
            
            if (lastTrack.getEnd().getId() == station.getPrevious().getId()) {
                lastTrack.setEnd(station);
                lastTrack.setOffDate(action.getTimestamp());
                lastTrack.setDistance((float) (lastTrack.getDistance() + station.getDistance()));
            } else {
                vehicleRepository.save(vehicle);
                try {
					 trackRepository.deleteById(lastTrack.getTid());
				} catch (Exception e) {
					logger.error("Cannot delete Track {}",lastTrack);
				}
               
                vehicle.getTracks().remove(lastTrack);
                return "falscher Vorgänger 2";
            }
            
          
        }

        vehicleRepository.save(vehicle);
        trackRepository.save(lastTrack);

        return "true";
        
    }
}