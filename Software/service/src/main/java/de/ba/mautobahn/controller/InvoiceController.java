package de.ba.mautobahn.controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.transform.TransformerException;

import org.apache.commons.io.IOUtils;
import org.apache.fop.apps.FOPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.Invoice;
import de.ba.mautobahn.models.Station;
import de.ba.mautobahn.models.User;
import de.ba.mautobahn.repositories.InvoiceRepository;
import de.ba.mautobahn.repositories.UserRepository;
import de.ba.mautobahn.services.InvoiceService;

@RestController
@RequestMapping("/invoice")
@CrossOrigin(origins = "http://localhost:3000")
public class InvoiceController {

	
	private InvoiceService invServ;
	private UserRepository userRep;
	private InvoiceRepository invRep;
	
	@Autowired
	public InvoiceController(InvoiceService invServ, UserRepository userRep, InvoiceRepository invRep) {
		super();
		this.invServ = invServ;
		this.userRep = userRep;
		this.invRep = invRep;
	}

	@GetMapping("/download/{ID}/{year}/{month}")
	public ResponseEntity<InputStreamResource> download(@PathVariable Long ID,@PathVariable int year,@PathVariable int month) throws IOException, FOPException, TransformerException {
		System.out.println(System.getProperty("user.dir"));
		
		InputStreamResource resource;
		
		//File file = new File("./src/main/resources/Invoicetest_"+ID+"_"+year+"_"+month+".pdf");
		File file = new File("./Invoices/Invoicetest_"+ID+"_"+year+"_"+month+".pdf");
		if (file.exists()) {
			  resource = new InputStreamResource(new FileInputStream(file));
			    return ResponseEntity.ok()
			            .contentLength(file.length())
			            .contentType(MediaType.APPLICATION_PDF)
			            .body(resource);
		} else {
			
				Optional<User> u = userRep.findById(ID);
				if(!u.isPresent()) return null;
				User user = u.get();
				invServ.createInvoiceForUser(user, year, month);
				resource = new InputStreamResource(new FileInputStream(file));
				 if(!file.exists()) {
				    	
				    	return ResponseEntity.badRequest().body(resource);
				    	
				    }
				   return ResponseEntity.ok()
				           .contentLength(file.length())
				           .contentType(MediaType.APPLICATION_PDF)
				           .body(resource);
			
		}
	}
	
	@GetMapping("/{ID}/all")
	public List<Invoice> getAllInvoices(@PathVariable Long ID) {
		List<Invoice> all = new ArrayList<>();
		
		for(Invoice i : invRep.findInvoicesByUserID(ID)) {
			all.add(i);
		}
		
		return all;
	}
	

}