package de.ba.mautobahn.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.CrossOrigin;

@Entity
@Table(name = "statistics")
public class Statistics {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private int foreignCars;
	
	@Column
	private int domesticCars;

	public Statistics() {
		
	}
	
	public Statistics(long id, int foreignCars, int domesticCars) {
		super();
		this.id = id;
		this.foreignCars = foreignCars;
		this.domesticCars = domesticCars;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getForeignCars() {
		return foreignCars;
	}

	public void setForeignCars(int foreignCars) {
		this.foreignCars = foreignCars;
	}

	public int getDomesticCars() {
		return domesticCars;
	}

	public void setDomesticCars(int domesticCars) {
		this.domesticCars = domesticCars;
	}
	
	
}
