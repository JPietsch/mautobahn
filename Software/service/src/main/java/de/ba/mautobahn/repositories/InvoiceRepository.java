package de.ba.mautobahn.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.Action;
import de.ba.mautobahn.models.Invoice;
import de.ba.mautobahn.models.Track;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

    @Query(value = "SELECT a FROM Invoice a left join fetch a.user where userid =?1")
    List<Invoice> findInvoicesByUserID(Long ID);
}