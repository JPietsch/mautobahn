package de.ba.mautobahn.services;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import de.ba.mautobahn.models.User;
import de.ba.mautobahn.repositories.AddressRepository;
import de.ba.mautobahn.repositories.UserRepository;

@Service
public class UserService {

	private Logger log = LoggerFactory.getLogger(UserService.class);
	
	private UserRepository userRepository;
	private AddressRepository addressRepository;
	private PasswordEncoder passwordEncoder;

	@Autowired
	public UserService(UserRepository userRepository, AddressRepository addressRepository,
			PasswordEncoder passwordEncoder) {
		super();
		this.userRepository = userRepository;
		this.addressRepository = addressRepository;
		this.passwordEncoder = passwordEncoder;
	}
	
	public void save(User user) {
		if(user == null) return;
				
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		
		
		userRepository.save(user);
		log.info("Save user: {}",userRepository.findById(user.getId()));
	}


}
