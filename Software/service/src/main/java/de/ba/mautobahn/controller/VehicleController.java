package de.ba.mautobahn.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ba.mautobahn.models.Vehicle;
import de.ba.mautobahn.models.dto.VehicleDTO;
import de.ba.mautobahn.repositories.VehicleRepository;
import de.ba.mautobahn.services.VehicleService;

@RestController
@RequestMapping("/vehicle")
@CrossOrigin(origins = "http://localhost:3000")
public class VehicleController {
	private VehicleRepository vehicleRepository;
	private VehicleService vehicleService;
	
	@Autowired
	public VehicleController(VehicleRepository vehicleRepository, VehicleService vehicleService) {
		super();
		this.vehicleRepository = vehicleRepository;
		this.vehicleService = vehicleService;
	}

	@GetMapping("/{id}")
	public Vehicle getVehicleById(@PathVariable String id) {
		Optional<Vehicle> entry = vehicleRepository.findById(id);
		
		return entry.get();
	}
	


	@GetMapping("/all")
	public List<VehicleDTO> getAllVehicle() {
		List<VehicleDTO> all = new ArrayList<>();
		
		for(Vehicle v : vehicleRepository.findAll()) {
			all.add(new VehicleDTO(v.getLicensePlateCode(), v.isOnRoad(), v.getOwner().getId(), v.getOwner()));
		}
		
		return all;
	}
	
	@PostMapping("/save")
	public boolean saveVehicle(@RequestBody @Validated VehicleDTO vehicle) {
		vehicleService.save(vehicle);
		return false;
	}
	
	
	@GetMapping("/delete/{lpc}")
	public boolean deleteUserById(@PathVariable String lpc) {
		System.out.println(lpc);
		try {			
			vehicleRepository.deleteByLicensePlateCode(lpc);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
