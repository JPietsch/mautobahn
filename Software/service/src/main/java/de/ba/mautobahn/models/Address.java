package de.ba.mautobahn.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address {
	
//	@Entity
//	@Table(name = "city")
//	public class City {
//		
//	    @Id
//	    @GeneratedValue(strategy=GenerationType.AUTO)
//	    private Long id;
//
//		@Column
//		private String name;
//		
//		@Column
//		private String postalCode;
//		
//		public City() {
//	
//		}
//		
//		public City(String name, String postalCode) {
//			super();
//			this.name = name;
//			this.postalCode = postalCode;
//		}
//		
//		public String getName() {
//			return name;
//		}
//		public void setName(String name) {
//			this.name = name;
//		}
//		public String getPostalCode() {
//			return postalCode;
//		}
//		public void setPostalCode(String postalCode) {
//			this.postalCode = postalCode;
//		}
//
//		@Override
//		public String toString() {
//			return "City [name=" + name + ", postalCode=" + postalCode + "]";
//		}
//				
//	}
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;


	@Column
	private String street;
	
	@Column
	private String houseNumber;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private City city;
	
	public Address() {
	
	}

	public Address(String street, String houseNumber, City city) {
		super();
		this.street = street;
		this.houseNumber = houseNumber;
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", houseNumber=" + houseNumber + ", city=" + city + "]";
	}
		
}
