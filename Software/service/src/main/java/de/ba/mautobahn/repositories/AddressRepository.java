package de.ba.mautobahn.repositories;

import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.Address;

public interface AddressRepository extends CrudRepository<Address, Long> {

}
