package de.ba.mautobahn.models;

public class InfoResponse {
	
	private String info = "Demo Info";

	public InfoResponse(String info) {
		super();
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "InfoResponse [info=" + info + "]";
	}
	
	
}
