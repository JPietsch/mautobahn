package de.ba.mautobahn.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mifmif.common.regex.util.Iterator;

import de.ba.mautobahn.models.Address;
import de.ba.mautobahn.models.Invoice;
import de.ba.mautobahn.models.InvoicePosition;
import de.ba.mautobahn.models.Track;
import de.ba.mautobahn.models.User;
import de.ba.mautobahn.repositories.InvoiceRepository;
import de.ba.mautobahn.repositories.TrackRepository;
import de.ba.mautobahn.repositories.UserRepository;
import de.ba.mautobahn.repositories.VehicleRepository;

@Service
public class InvoiceService {

	private UserRepository userRepository;
	private VehicleRepository vehicleRepository;
	private TrackRepository trackRepository;
	private InvoiceRepository invoiceRepository;
	private List<Track> temp = new ArrayList<>();
	private List<Long> userIDs = new ArrayList<>();
	private String xmlTemp;
	private User userTemp;
	
	private int year;
	private int month;
	
	private static double taxrate = 1.1;
	private static double pricePerKM = 0.05;

	@Autowired
	public InvoiceService(UserRepository userRepository, VehicleRepository vehicleRepository,
			TrackRepository trackRepository, InvoiceRepository invoiceRepository) {
		super();
		this.userRepository = userRepository;
		this.vehicleRepository = vehicleRepository;
		this.trackRepository = trackRepository;
		this.invoiceRepository = invoiceRepository;
	}

//	public Invoice createInvoice() {
//		// create Hashmap for Tracks with licenseplate mapping
//		// calc. price per Track
//		Double nettoSum = 0.0;
//		Double bruttoSum = 0.0;
//		String user;
//		Address adr;
//		HashMap<String, List<Track>> tracktable = new HashMap<String, List<Track>>();
//		List<Track> tl = trackRepository.findInvoiceTracks1();
//		user = tl.get(0).getvehicle().getOwner().getFirstName();
//		adr = tl.get(0).getvehicle().getOwner().getAddress();
//		for (Track t : tl) {
//			t.setPrice(t.getDistance() * InvoiceService.pricePerKM);
//			nettoSum += t.getPrice();
//			if (tracktable.containsKey(t.getvehicle().getLicensePlateCode())) {
//				temp = tracktable.get(t.getvehicle().getLicensePlateCode());
//				temp.add(t);
//				tracktable.put(t.getvehicle().getLicensePlateCode(), temp);
//			} else {
//				temp = new ArrayList<>();
//				temp.add(t);
//				tracktable.put(t.getvehicle().getLicensePlateCode(), temp);
//			}
//
//		}
//		bruttoSum = nettoSum * InvoiceService.taxrate;
//		return new Invoice(user, adr, taxrate, bruttoSum, nettoSum, tracktable);
//	}

	public void createInvoiceForEveryUser(int year, int month) throws FOPException, IOException, TransformerException {
		
		this.year = year;
		this.month = month;
		Iterable<User> it = userRepository.findAll();
	
		for (User u : it) {
			
			if (u == null)  continue;
			System.out.println(u.getId());
			Invoice invTemp = createInvoice(u);
			if(invTemp == null) continue;
				try {
					
					xmlTemp = createXMLfromInvoice(invTemp);
					convertToPdfwithXmlString(xmlTemp,u.getId());
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			
				
		}
	}
	
	public void createInvoiceForUser(User u, int year, int month) throws FOPException, IOException, TransformerException {
			

			this.year = year;
			this.month = month;
			
			if (u != null) {
				
			System.out.println(u.getId());
			Invoice invTemp = createInvoice(u);
			System.out.println(invTemp);
			
				try {
					xmlTemp = createXMLfromInvoice(invTemp);
					convertToPdfwithXmlString(xmlTemp,u.getId());
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			}
		
	}
	
	
	
	
	
	private Invoice createInvoice(User user) {
		
		Double nettoSum = 0.0;
		Double bruttoSum = 0.0;
		
		List<InvoicePosition> invPosList = new ArrayList<>();
		List<Track> tl = trackRepository.findInvoiceTracksUserDate(user.getId(), year,month);

		//List<Track> tl = trackRepository.findInvoiceTracks1(user.getId());
		if (tl.isEmpty()) return null;
		Invoice invoice = new Invoice();
		
//		invoice.setuserID(user.getId());
		invoice.setUser(user);
		
		LocalDate ldt = LocalDate.now();
		
		invoice.setRDatum(ldt);
		
		for (Track t : tl) {
			t.setPrice(t.getDistance() * InvoiceService.pricePerKM);
			nettoSum += t.getPrice();
			InvoicePosition ip = getInvoicePosition(invPosList, t);
			if (ip==null) continue;
			if (!invPosList.contains(ip)) {
				invPosList.add(ip);
			} else {
				ip.addTrack(t);
			}
		}
		
		bruttoSum = nettoSum * InvoiceService.taxrate;
		invoice.setBruttoSumme(bruttoSum);
		invoice.setNettoSumme(nettoSum);
		invoice.setSteuersatz(taxrate);
		invoice.setUser(user);
		invoice.setInvoicePosition(invPosList);
		invoiceRepository.save(invoice);
		return invoice;
	}

	private InvoicePosition getInvoicePosition(List<InvoicePosition> invPosList, Track t) {
		
		if (invPosList==null) return null;
		if (t==null) return null;
		
		for (InvoicePosition invoicePosition : invPosList) {
			if (invoicePosition.getLicensePlateCode() == t.getVehicle().getLicensePlateCode()) {

				return invoicePosition;

			}
		}
		List<Track> tracks = new ArrayList<Track>();
		tracks.add(t);
		return new InvoicePosition(tracks, t.getVehicle().getLicensePlateCode(), invPosList.size()+1);
	}

	private String createXMLfromInvoice(Invoice inv) throws JsonProcessingException {

		XmlMapper xmlMapper = new XmlMapper();
		xmlMapper.registerModule(new JavaTimeModule());
		String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(inv);
		System.out.println(xml);
		return xml;
	}
	
	private void convertToPdfwithXmlString(String xml, Long ID) throws IOException, FOPException, TransformerException {
		// the XSL FO file
		// after http://xmlgraphics.apache.org/fop/2.6/embedding.html
		File xsltFile = new File("./src/main/resources/template.xsl");
		// the XML file which provides the input
		
		//StreamSource xmlSource = new StreamSource(new File("./src/main/resources/Invoice.xml"));
		
		// create an instance of fop factory
		StreamSource xml1 = new StreamSource(new StringReader(xml));
		
		FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
		// a user agent is needed for transformation
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		// Setup output
		OutputStream out;
//		out = new java.io.FileOutputStream("./src/main/resources/Invoicetest_"+ID+"_"+year+"_"+month+".pdf");
		out = new java.io.FileOutputStream("./Invoices/Invoicetest_"+ID+"_"+year+"_"+month+".pdf");

		try {
			// Construct fop with desired output format
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

			// Setup XSLT
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

			// Resulting SAX events (the generated FO) must be piped through to FOP
			Result res = new SAXResult(fop.getDefaultHandler());

			// Start XSLT transformation and FOP processing
			// That's where the XML is first transformed to XSL-FO and then
			// PDF is created
			transformer.transform(xml1, res);
		} finally {
			out.close();
		}

	}

	public void convertToPDF() throws IOException, FOPException, TransformerException {
		// the XSL FO file
		// after http://xmlgraphics.apache.org/fop/2.6/embedding.html
		File xsltFile = new File("./src/main/resources/template.xsl");
		// the XML file which provides the input
		StreamSource xmlSource = new StreamSource(new File("./src/main/resources/Invoice.xml"));
		// create an instance of fop factory
		FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
		// a user agent is needed for transformation
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		// Setup output
		OutputStream out;
		out = new java.io.FileOutputStream("./src/main/resources/Invoice.pdf");

		try {
			// Construct fop with desired output format
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

			// Setup XSLT
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));

			// Resulting SAX events (the generated FO) must be piped through to FOP
			Result res = new SAXResult(fop.getDefaultHandler());

			// Start XSLT transformation and FOP processing
			// That's where the XML is first transformed to XSL-FO and then
			// PDF is created
			transformer.transform(xmlSource, res);
		} finally {
			out.close();
		}

	}
}