package de.ba.mautobahn.repositories;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import de.ba.mautobahn.models.Track;


public interface TrackRepository extends CrudRepository<Track, Long>{

	
	@Query(value = "SELECT * "
			+ "FROM TRACK "
			+ "WHERE OFF_DATE BETWEEN ?1 AND ?2", nativeQuery = true)
	List<Track> findInTimeRange(LocalDateTime begin, LocalDateTime end);
	

	@Query(value = "SELECT * FROM TRACK left join vehicle on track.vehicle = vehicle.license_plate_code Left join Users On Users.ID = vehicle.owner where Year(OFF_Date) = 2019 AND Month(OFF_Date) = 10 AND (users.id=1 OR users.id=1)", nativeQuery = true)
	List<Track> findInvoiceTracks();
	
	@Query(value = "SELECT a FROM Track a left join fetch a.vehicle where Year(OFF_Date) = 2019 AND Month(OFF_Date) = 10 AND owner=815")
	List<Track> findInvoiceTracks1();
	
	@Query(value = "SELECT a FROM Track a left join fetch a.vehicle where Year(OFF_Date) = ?2 AND Month(OFF_Date) = ?3 AND owner=?1")
    List<Track> findInvoiceTracksUserDate(Long ID, int year, int month);
	
}
