package de.ba.mautobahn.models;

public enum StationType {
	DEPARTURE, PASSAGE;
}
