package de.ba.mautobahn;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import de.ba.mautobahn.models.User;
import de.ba.mautobahn.repositories.UserRepository;

@SpringBootApplication
public class MautobahnApplication  {

	public static void main(String[] args) {
		SpringApplication.run(MautobahnApplication.class, args);
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/user/*").allowedOrigins("http://localhost:3000");
			}
		};
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
//		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
		return new BCryptPasswordEncoder();
	}
	

	@Bean
	CommandLineRunner init(UserRepository userRepository) {
		return (evt) -> {
			if(userRepository.findByUsername("admin").isEmpty()) {
				User admin = new User("admin", passwordEncoder().encode("admin"), new String[]{"ADMIN", "USER"});
				userRepository.save(admin);		
			}
			if(userRepository.findByUsername("user").isEmpty()) {
				User user = new User("user", passwordEncoder().encode("user"), new String[]{"USER"});
				userRepository.save(user);		
			}
		};
	}
	
	
//	@Bean
//	CommandLineRunner runner() {
//		return args -> {
//			userRepository.save(new User("$2a$10$JIUpIpc7xawszEtzOXFBb.Iue7vYtcro1Y.OFBofx0SdV3q4MspOS", "USER", "Max", "Mustermann"));
//			userRepository.save(new User("$2a$10$Qsb3ISEzypEQI8sawGONwOX8skDou.C7GYySoPXzvdzRrW12K9pIW", "ADMIN", "Erika", "Mustermann"));
//		};
//	}
}
