package de.ba.mautobahn.models;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "action")
public class Action {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@Column
	private String licensePlateCode;
	
	@Column
	private Long stationId;
	
	@Column
	private LocalDateTime timestamp;

	public Action() {}
	
	public Action(String licensePlateCode, Long stationId, LocalDateTime timestamp) {
		super();
		this.licensePlateCode = licensePlateCode;
		this.stationId = stationId;
		this.timestamp = timestamp;
	}

	public String getLicensePlateCode() {
		return licensePlateCode;
	}

	public void setLicensePlateCode(String licensePlateCode) {
		this.licensePlateCode = licensePlateCode;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Action [licensePlateCode=" + licensePlateCode + ", stationId=" + stationId + ", timestamp=" + timestamp
				+ "]";
	}
		
}
