package de.ba.mautobahn.services;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import de.ba.mautobahn.models.dto.JWTTokenResponse;
import de.ba.mautobahn.repositories.UserRepository;

@Service
public class AuthenticationService {

	private UserRepository userRepository;
	private JwtTokenService jwtTokenService;
	private PasswordEncoder passwordEncoder;

	@Autowired
	public AuthenticationService(UserRepository userRepository, JwtTokenService jwtTokenService,
			PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.jwtTokenService = jwtTokenService;
		this.passwordEncoder = passwordEncoder;
	}

	public JWTTokenResponse generateJWTToken(String username, String password) {
		return userRepository.findByUsername(username)
				.filter(user -> passwordEncoder.matches(password, user.getPassword()))
				.map(user -> new JWTTokenResponse(jwtTokenService.generateToken(user)))
				.orElseThrow(() -> new EntityNotFoundException("User not found or worng password!"));
	}

}
