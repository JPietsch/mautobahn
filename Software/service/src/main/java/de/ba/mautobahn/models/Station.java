package de.ba.mautobahn.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "station")
public class Station {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column
    private String title;
    
    @Column
    private StationType type;
    
    @Column
    private double distance;
    
    @OneToOne
    private Station previous;
    
    @Column
    private double longitude;
    
    @Column
    private double latitude;
 
    public Station() {}

	public Station(Long id, String title, StationType type, double distance, Station previous, double longditude,
			double latitude) {
		super();
		this.id = id;
		this.title = title;
		this.type = type;
		this.distance = distance;
		this.previous = previous;
		this.longitude = longditude;
		this.latitude = latitude;
	}

	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Station getPrevious() {
		return previous;
	}


	public void setPrevious(Station previous) {
		this.previous = previous;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public StationType getType() {
		return type;
	}

	public void setType(StationType type) {
		this.type = type;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longditude) {
		this.longitude = longditude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "Station [id=" + id + ", title=" + title + ", type=" + type + ", distance=" + distance + ", previous="
				+ previous + ", longditude=" + longitude + ", latitude=" + latitude + "]";
	}	

}
