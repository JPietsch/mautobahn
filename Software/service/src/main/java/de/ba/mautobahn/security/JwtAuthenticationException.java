package de.ba.mautobahn.security;

import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationException extends AuthenticationException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7918003547173093496L;

	public JwtAuthenticationException(String msg) {
        super(msg);
    }
}
