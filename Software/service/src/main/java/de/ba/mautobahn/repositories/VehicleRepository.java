package de.ba.mautobahn.repositories;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import de.ba.mautobahn.models.Vehicle;

public interface VehicleRepository extends CrudRepository<Vehicle, String>{
	Vehicle findByLicensePlateCode(String licensePlateCode);

	@Query(value = "SELECT * FROM Vehicle left join Track on Vehicle.License_Plate_Code = Track.Vehicle Where Track.OFF_DATE BETWEEN 20161023 AND 20161023" , nativeQuery = true)
	Vehicle getInvoiceVehicle();

	@Modifying
	@Transactional
	@Query(value = "DELETE "
			+ "FROM VEHICLE "
			+ "WHERE LICENSE_PLATE_CODE = ?1", nativeQuery = true)
	//@Modifying
	//@Query("delete from Vehicle v where v.licensePlateCode=:licensePlateCode")
	void deleteByLicensePlateCode(@Param("licensePlateCode") String licensePlateCode);
  
	
}
