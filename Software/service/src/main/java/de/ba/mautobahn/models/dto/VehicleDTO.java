package de.ba.mautobahn.models.dto;

import de.ba.mautobahn.models.User;

public class VehicleDTO {
	
	private String licensePlateCode;
	private boolean isOnRoad;
	private long owner;
	private User user;

	public VehicleDTO() {}

	public VehicleDTO(String licensePlateCode, boolean isOnRoad, Long owner, User user) {
		super();
		this.licensePlateCode = licensePlateCode;
		this.isOnRoad = isOnRoad;
		this.owner = owner;
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getLicensePlateCode() {
		return licensePlateCode;
	}

	public void setLicensePlateCode(String licensePlateCode) {
		this.licensePlateCode = licensePlateCode;
	}

	public boolean isOnRoad() {
		return isOnRoad;
	}

	public void setOnRoad(boolean isOnRoad) {
		this.isOnRoad = isOnRoad;
	}

	public long getOwner() {
		return owner;
	}

	public void setOwner(long owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "VehicleDTO [licensePlateCode=" + licensePlateCode + ", isOnRoad=" + isOnRoad + ", owner=" + owner
				+ ", user=" + user + "]";
	}
	
}
