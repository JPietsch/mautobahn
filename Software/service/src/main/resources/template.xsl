﻿<?xml version="1.0" encoding="UTF-8" ?>
 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" omit-xml-declaration="no" version="1.0" encoding="UTF-8" indent="yes" />
     
    <xsl:template match="/">
        <fo:root>
             <fo:layout-master-set>
                <fo:simple-page-master master-name="DIN-A4" page-height="297mm" page-width="210mm" margin-top="10mm">
                    <fo:region-body region-name="inhalt" margin="30mm" margin-top="80mm"/>
                    <fo:region-before region-name="kopf" extent="80mm"  />
                    <fo:region-after region-name="fuss" extent="30mm"  />
                    <fo:region-start extent="25mm"  />
                    <fo:region-end extent="25mm"  />
                </fo:simple-page-master>
            </fo:layout-master-set>
             
			 <fo:page-sequence master-reference="DIN-A4">
			 <fo:static-content flow-name="kopf">
                    
					<fo:table font-family="Helvetica" font-size="10px">
                        <fo:table-column column-width="50%" border-style="none" border-width="0.25mm" border-left-width="0.5mm" />
                        <fo:table-column column-width="50%" border-style="none" border-width="0.25mm" />
                        
                        <fo:table-body> 
                            <fo:table-row border-style="none" border-width="0.25mm" font-size="10px">
								<fo:table-cell>
									<fo:block text-align="right">
									</fo:block>
								</fo:table-cell>
								
								<fo:table-cell>
									<fo:block text-align="right">
									<fo:external-graphic src="url('file:./Logo.jpg')" /> 
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row border-style="none" border-width="0.25mm" font-size="10px">
								
								<fo:table-cell>
									<xsl:apply-templates select="/Invoice/user"/>
									<fo:block text-align="left">
									</fo:block>
									<fo:block text-align="left">
										
									</fo:block>
									<fo:block text-align="left">
										<xsl:value-of select="/Invoice/rdatum[3]" />.<xsl:value-of select="/Invoice/rdatum[2]" />.<xsl:value-of select="/Invoice/rdatum[1]" />
									</fo:block>
								</fo:table-cell>
								<fo:table-cell>
									<fo:block text-align="right">
									Darth Maut GmbH
									</fo:block>
									<fo:block text-align="right">
									Löbauer Str. 1
									</fo:block>
									<fo:block text-align="right">
									02625 Bautzen
									</fo:block>
									<fo:block text-align="right">
									Telefon: +49 (0) 3591/000000 
									
									</fo:block>
									
									<fo:block text-align="right">
									Telefax: +49 (0) 3591/000000
									
									</fo:block>
									<fo:block text-align="right">
									 
									E-Mail: info@darth-maut.com
									
									</fo:block>
									<fo:block text-align="right">
									Internet: www.darth-maut.com
									
									</fo:block>
									
								</fo:table-cell>
							
							</fo:table-row>
                        </fo:table-body>
                    </fo:table>
			
			  </fo:static-content>
<fo:static-content flow-name="fuss">

<fo:table font-family="Helvetica" font-size="8px" >
                        <fo:table-column column-width="35%" border-style="none" border-width="0.25mm" border-left-width="0.5mm" />
                        <fo:table-column column-width="65%" border-style="none" border-width="0.25mm" />
                        
                        <fo:table-body>
                          
							<fo:table-row border-style="none" border-width="0.25mm" font-size="10px">
								
								<fo:table-cell><fo:block color="#888888">
									Darth Maut GmbH 
Geschäftsführer: Marc-Phillip Panten 
Eingetragen im Handelsregister der Stadt 
Dresden unter der Nummer HRB xxxxx 
USt-IdNr.: DE999999999 </fo:block>
								</fo:table-cell>
								<fo:table-cell>
									
									<fo:block text-align="right" color="#888888"><fo:inline>Bankverbindung: </fo:inline></fo:block>
									<fo:block text-align="right" color="#888888"><fo:inline>123 Bank </fo:inline></fo:block>
									<fo:block text-align="right" color="#888888"><fo:inline>Zahlungsempfänger: Darth Maut GmbH </fo:inline></fo:block>
									<fo:block text-align="right" color="#888888"><fo:inline>IBAN: DE12 3456 7890 1234 5678 90 </fo:inline></fo:block>
									<fo:block text-align="right" color="#888888"><fo:inline>BIC: WELADED1GRL </fo:inline></fo:block>
								</fo:table-cell>
							
							</fo:table-row>
                        </fo:table-body>
                    </fo:table>



</fo:static-content>
			
			
			
                <fo:flow flow-name="inhalt">
                    <fo:table font-family="Helvetica">
                        <fo:table-column column-width="5%" border-style="none" border-width="0.25mm" border-left-width="0.5mm" />
                        <fo:table-column column-width="25%" border-style="none" border-width="0.25mm" />
                        <fo:table-column column-width="40%" border-style="none" border-width="0.25mm" border-right-width="0.5mm" />
						<fo:table-column column-width="15%" border-style="none" border-width="0.25mm" border-right-width="0.5mm" />
						<fo:table-column column-width="15%" border-style="none" border-width="0.25mm" border-right-width="0.5mm" />
                        <fo:table-header>
                            <fo:table-row background-color="#CCCCCC" text-align="left" font-weight="bold" font-size="13px">
                                <fo:table-cell number-columns-spanned="5">
                                   
									<fo:block line-height="150%"> <xsl:value-of select="('Rechnung ')" /><xsl:value-of select="Invoice/invoiceID" /></fo:block>
									<fo:block>
									</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row background-color="#FFFFFF" border-style="none" border-bottom="solid" border-width="0.25mm" text-align="center" font-weight="bold" font-size="11px">
                                
								 <fo:table-cell>
                                    <fo:block>Pos.</fo:block>
                                </fo:table-cell>
								<fo:table-cell>
                                    <fo:block>Fahrzeug</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>Strecke</fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block>Länge in Km</fo:block>
                                </fo:table-cell>
                                 <fo:table-cell>
                                    <fo:block>Preis</fo:block>
                                    <fo:block>in €</fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-header>
                        <fo:table-body>
                            <xsl:apply-templates select="/Invoice/InvoicePositions/InvoicePosition"/>
                            <xsl:apply-templates select="/Invoice"></xsl:apply-templates>
                        </fo:table-body>
                    </fo:table>
                     
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
     
    <xsl:template match="/Invoice/InvoicePositions/InvoicePosition">
        <fo:table-row border-style="none" border-width="0.25mm" font-size="11px" text-align="center">
            <fo:table-cell>
                <fo:block>
                    <xsl:value-of select="pos" />
                </fo:block>
            </fo:table-cell>
			<fo:table-cell>
                <fo:block>
                    <xsl:value-of select="licensePlateCode" />
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    
                </fo:block>
            </fo:table-cell>
			<fo:table-cell>
                <fo:block>
                    
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
        
        
        
        		
										
										<xsl:for-each select="tracks/track">
										
											<fo:table-row border-style="none" border-width="0.25mm" font-size="12px" text-align="center">
			
			
														<fo:table-cell>
										                <fo:block>
										                    
										                </fo:block>
										            </fo:table-cell>
													<fo:table-cell>
										                <fo:block>
										                    
										                </fo:block>
										            </fo:table-cell>
										       
										            <fo:table-cell>
										            	
										                <fo:block>
										                    <xsl:value-of select="('Von ')"/> <xsl:value-of select="start/title" /> 
										                </fo:block>
										                
										           		<fo:block>
										                     <xsl:value-of select="(' nach ')"/><xsl:value-of select="end/title" />
										                </fo:block>
										            </fo:table-cell>
										            <fo:table-cell>
										                <fo:block>
										                    <xsl:value-of select="distance" />
										                </fo:block>
										            </fo:table-cell>
										              <fo:table-cell>
										                <fo:block>
										                    <xsl:value-of select="price" />
										                </fo:block>
										            </fo:table-cell>
													
													
										      </fo:table-row>
										
											
										</xsl:for-each>
						
					
					
	
		
    </xsl:template>
	
	
	
	<xsl:template match="/Invoice">
			
 		<fo:table-row background-color="#CCCCCC" font-weight="normal"  font-size="13px">
             <fo:table-cell number-columns-spanned="3">
				<fo:block text-align="right">
					Nettobetrag
				</fo:block>
				<fo:block text-align="right">
					Steuersatz
				</fo:block>
				<fo:block text-align="right" font-weight="bold">
					Bruttobetrag
				</fo:block>
	             </fo:table-cell>
	             <fo:table-cell number-columns-spanned="2" >
				<fo:block text-align="right">
					<xsl:value-of select="nettoSumme" />
				</fo:block>
				<fo:block text-align="right">
					<xsl:value-of select="steuersatz" />
				</fo:block>						
				<fo:block font-weight="bold" text-align="right">
					<xsl:value-of select="bruttoSumme" />
				</fo:block>	
             </fo:table-cell>
            
            </fo:table-row>
		
    </xsl:template>
	
	<xsl:template match="/Invoice/user">
				<fo:block>
                    	<xsl:value-of select="lastName" /><xsl:value-of select="(' ')" /><xsl:value-of select="firstName" />
                </fo:block>
				<fo:block>
                    <xsl:value-of select="email" />
                </fo:block>
				<fo:block>
                    <xsl:apply-templates select="/Invoice/user/address"/>
                </fo:block>
    </xsl:template>
	
	<xsl:template match="/Invoice/user/address">
				<fo:block>
                    	<xsl:value-of select="street" /> <xsl:value-of select="houseNumber" />
                </fo:block>
				<fo:block>
                    	<xsl:value-of select="city" />
                </fo:block>
    </xsl:template>
	
	
	
</xsl:stylesheet>